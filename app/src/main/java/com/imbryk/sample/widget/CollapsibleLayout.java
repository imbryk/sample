package com.imbryk.sample.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ScrollerCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.imbryk.sample.R;

public class CollapsibleLayout extends ViewGroup {

    private static final int DEFAULT_COLLAPSED_TOTAL_HEIGHT = 200; //dp
    private static final int[] DRAWABLE_STATE_COLLAPSED = {R.attr.collapsible_state_collapsed};

    private ScrollerCompat mScroller;
    private int mExpandedContentHeight = 0;
    private int mCollapsedContentHeight = 0;
    private int mCollapsedTotalHeightToReturn = 0;
    private int mCollapsedTotalHeight;

    private boolean mAlignToLines = true;
    private boolean mIsCollapsed = true;
    private boolean mIsCollapsible = true;
    private int mMaxHeaderHeight = 0;
    private int mMaxFooterHeight = 0;

    private int mAnimationDuration = 500;

    private boolean mIsAnimating = false;
    private boolean mInLayout = false;

    private boolean mHasClickToggleResId = false;
    private int mClickToggleResId;

    private View mInnerToggle;
    private View mOuterToggle;

    private boolean mHasRequestedLayoutDelayed = false;

    private OnCollapseListener mListener;

    private OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            toggle();
        }
    };
    private boolean mIsClickable;
    private int mSetScrollOnLayout;
    private int mCurrY;


    public int getCollapsedHeight() {
        return mCollapsedTotalHeightToReturn;
    }

    public void setOnCollapseListener(OnCollapseListener listener) {
        mListener = listener;
    }

    public boolean isCollapsed() {
        return mIsCollapsed;
    }

    public void toggle() {
        if (mScroller.getFinalY() == mExpandedContentHeight) {
            collapse();
        } else {
            expand();
        }
    }

    public void expand() {
        if (mIsCollapsible) {
            if (mScroller.getFinalY() != mExpandedContentHeight) {
                mCurrY = mScroller.getCurrY();
                mIsAnimating = true;
                int dy = mExpandedContentHeight - mCurrY;
                int maxDy = mExpandedContentHeight - mCollapsedContentHeight;

                float prop = Math.abs((float) dy / (float) maxDy);

                mScroller.startScroll(0, mCurrY, 0, dy, (int) (prop * mAnimationDuration));
            }
            mIsCollapsed = false;
            refreshDrawableState();
            requestLayoutDelayed();
        }
    }

    public void collapse() {
        if (mIsCollapsible) {
            if (mScroller.getFinalY() != mCollapsedContentHeight) {
                mCurrY = mScroller.getCurrY();
                mIsAnimating = true;
                int dy = mCollapsedContentHeight - mCurrY;
                int maxDy = mExpandedContentHeight - mCollapsedContentHeight;

                float prop = Math.abs((float) dy / (float) maxDy);

                mScroller.startScroll(0, mCurrY, 0, dy, (int) (prop * mAnimationDuration));
            }
            mIsCollapsed = true;
            refreshDrawableState();
            requestLayoutDelayed();
        }
    }

    private void requestLayoutDelayed() {
        if (mHasRequestedLayoutDelayed) {
            return;
        }
        mHasRequestedLayoutDelayed = true;
        postDelayed(new Runnable() {

            @Override
            public void run() {
                mHasRequestedLayoutDelayed = false;
                if (mInLayout) {
                    requestLayoutDelayed();
                } else {
                    if (mScroller.computeScrollOffset()) {
                        int currY = mScroller.getCurrY();

                        if (currY == mScroller.getFinalY() && mIsAnimating) {
                            if (currY == mExpandedContentHeight) {
                                mIsCollapsed = false;
                                if (mListener != null) {
                                    mListener.onExpanded(CollapsibleLayout.this);
                                }
                            } else if (currY == mCollapsedContentHeight) {
                                mIsCollapsed = true;
                                if (mListener != null) {
                                    mListener.onCollapsed(CollapsibleLayout.this);
                                }
                            }
                            mIsAnimating = false;
                            refreshDrawableState();
                        }
                        requestLayout();
                    } else if (mExpandedContentHeight == 0) {
                        requestLayout();
                    }
                }
            }
        }, 10);
    }

    public CollapsibleLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    public CollapsibleLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CollapsibleLayout(Context context) {
        this(context, null);
    }

    private void init(Context context, AttributeSet attrs) {
        mScroller = ScrollerCompat.create(context);

        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CollapsibleLayout);

        mIsClickable = a.getBoolean(R.styleable.CollapsibleLayout_android_clickable, true);

        mIsCollapsed = !a.getBoolean(R.styleable.CollapsibleLayout_is_expanded, false);

        mAlignToLines = a.getBoolean(R.styleable.CollapsibleLayout_align_to_lines, true);
        mAnimationDuration = a.getInt(R.styleable.CollapsibleLayout_animations_duration, mAnimationDuration);

        mHasClickToggleResId = a.hasValue(R.styleable.CollapsibleLayout_toggle_button);
        if (mHasClickToggleResId) {
            mClickToggleResId = a.getResourceId(R.styleable.CollapsibleLayout_toggle_button, -1);
        }

        mCollapsedTotalHeight = a.getDimensionPixelOffset(R.styleable.CollapsibleLayout_collapsed_height, -1);
        if (mCollapsedTotalHeight == -1) {
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            DisplayMetrics outMetrics = new DisplayMetrics();
            wm.getDefaultDisplay().getMetrics(outMetrics);
            mCollapsedTotalHeight = (int) (DEFAULT_COLLAPSED_TOTAL_HEIGHT * outMetrics.density);
        }

        a.recycle();

        if (mIsClickable) {
            setOnClickListener(onClickListener);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        if (mOuterToggle != null) {
            mOuterToggle.setOnClickListener(null);
        }
        super.onDetachedFromWindow();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        if (mHasClickToggleResId && mInnerToggle == null) {
            mOuterToggle = findViewById(mClickToggleResId);
            ViewParent parent = this;
            while (mOuterToggle == null && parent != null && parent instanceof View) {
                mOuterToggle = ((View) parent).findViewById(mClickToggleResId);
                parent = parent.getParent();
            }
            if (mOuterToggle != null) {
                mOuterToggle.setOnClickListener(onClickListener);
            }
        }
    }

    @Override
    protected void onFinishInflate() {
        checkValidChildren();
        if (mHasClickToggleResId) {
            mInnerToggle = findViewById(mClickToggleResId);
            if (mInnerToggle != null) {
                mInnerToggle.setOnClickListener(onClickListener);
            }
        }
        super.onFinishInflate();
    }

    private void checkValidChildren() {
        final int childCount = getChildCount();
        boolean hasCollapsableView = false;
        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);

            if (child.getVisibility() == GONE) {
                continue;
            }
            if (isCollapsibleView(child)) {
                if (hasCollapsableView) {
                    throw new IllegalStateException("only one child can have layout_gravity equal Gravity.NO_GRAVITY");
                }
                hasCollapsableView = true;
            } else if (!isHeaderView(child) && !isFooterView(child)) {
                throw new IllegalStateException("Child " + child + " at index " + i
                        + " does not have a valid layout_gravity - must be Gravity.TOP, "
                        + "Gravity.BOTTOM or Gravity.NO_GRAVITY");
            }
        }
    }

    @Override
    public void invalidate() {
        super.invalidate();
    }

    //*******************************************************************************************
    //* Measure
    //*******************************************************************************************
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        mSetScrollOnLayout = -1;
        final int paddingLeft = getPaddingLeft();
        final int paddingTop = getPaddingTop();
        final int paddingRight = getPaddingRight();
        final int paddingBottom = getPaddingBottom();

        int maxHeight = 0;
        int maxWidth = 0;
        final int childCount = getChildCount();

        int maxWidthCollapsable = 0;        //maximum view width when view is collapsable
        int maxWidthFixed = 0;                //maximum view width when view is NOT collapsable
        int maxFooterHeightCollapsable = 0;    //maximum footer height when view is collapsable
        int maxHeaderHeightCollapsable = 0;    //maximum header height when view is collapsable
        int maxFooterHeightFixed = 0;        //maximum footer height when view is NOT collapsable (is fixed)
        int maxHeaderHeightFixed = 0;        //maximum header height when view is NOT collapsable (is fixed)

        View collapsableView = null;
        boolean hasCollapsableView = false;
        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);

            final LayoutParams lp = (LayoutParams) child.getLayoutParams();
            boolean isCollapsableView = isCollapsibleView(child);
            boolean isHeaderView = isHeaderView(child);
            boolean isFooterView = isFooterView(child);


            if (!isFooterView && !isHeaderView && !isCollapsableView) {
                throw new IllegalStateException("Child " + child + " at index " + i
                        + " does not have a valid vertical layout_gravity - must be Gravity.TOP, "
                        + "Gravity.BOTTOM or Gravity.NO_GRAVITY");
            }

            if (isCollapsableView) {

                if (child.getVisibility() == GONE) {
                    continue;
                }
                if (hasCollapsableView) {
                    throw new IllegalStateException("only one child can have layout_gravity equal Gravity.NO_GRAVITY");
                }
                collapsableView = child;
                hasCollapsableView = true;
                //measure collapsable last, as we need footer and header heights
            } else {
                // if not animating measure all views in case headers/footers need to be shown
                if (mIsAnimating || child.getVisibility() == GONE) {

                    continue;
                }

                final int childWidthMeasureSpec = getChildMeasureSpec(widthMeasureSpec, paddingLeft + paddingRight
                        + lp.leftMargin + lp.rightMargin, lp.width);
                final int childHeightMeasureSpec = getChildMeasureSpec(heightMeasureSpec, paddingTop + paddingBottom
                        + lp.topMargin + lp.bottomMargin, lp.height);

                child.measure(childWidthMeasureSpec, childHeightMeasureSpec);

                if (lp.alwaysVisible) {
                    //below values are important only for views that are visible even when layout is NOT collapsable
                    maxWidthFixed = Math.max(maxWidthFixed, child.getMeasuredWidth() + lp.leftMargin + lp.rightMargin);
                    if (isHeaderView) {
                        maxHeaderHeightFixed = Math.max(maxHeaderHeightFixed, child.getMeasuredHeight() + lp.topMargin
                                + lp.bottomMargin);
                    } else if (isFooterView) {
                        maxFooterHeightFixed = Math.max(maxFooterHeightFixed, child.getMeasuredHeight() + lp.topMargin
                                + lp.bottomMargin);
                    }
                }
                maxWidthCollapsable = Math.max(maxWidthCollapsable, child.getMeasuredWidth() + lp.leftMargin + lp.rightMargin);
                if (isHeaderView) {
                    maxHeaderHeightCollapsable = Math.max(maxHeaderHeightCollapsable, child.getMeasuredHeight() + lp.topMargin
                            + lp.bottomMargin);
                } else if (isFooterView) {
                    maxFooterHeightCollapsable = Math.max(maxFooterHeightCollapsable, child.getMeasuredHeight() + lp.topMargin
                            + lp.bottomMargin);
                }
                //if layout is not collapsable then some of the headers/footers will be set to gone
            }
        }
        boolean updateViewsVisibility = false;
        if (hasCollapsableView) {

            final LayoutParams lp = (LayoutParams) collapsableView.getLayoutParams();

            final int childWidthMeasureSpec = getChildMeasureSpec(widthMeasureSpec, paddingLeft + paddingRight
                    + lp.leftMargin + lp.rightMargin, lp.width);
            final int childHeightMeasureSpec = getChildMeasureSpec(heightMeasureSpec, paddingTop + paddingBottom
                    + lp.topMargin + lp.bottomMargin, mScroller.getCurrY());

            if (!mIsAnimating) {
                final int expandedContentHeightMeasureSpec = getChildMeasureSpec(heightMeasureSpec, paddingTop
                        + paddingBottom + lp.topMargin + lp.bottomMargin, lp.height);
                collapsableView.measure(childWidthMeasureSpec, expandedContentHeightMeasureSpec);
                int newExpandedContentHeight = collapsableView.getMeasuredHeight();


                if (newExpandedContentHeight != mExpandedContentHeight) {

                    int expandedTotalHeightFixed = newExpandedContentHeight + lp.topMargin + lp.bottomMargin
                            + paddingTop + paddingBottom + maxFooterHeightFixed + maxHeaderHeightFixed;

                    if (expandedTotalHeightFixed <= mCollapsedTotalHeight) {
                        if (mIsCollapsible) {
                            updateViewsVisibility = true;
                            mIsCollapsible = false;
                        }
                    } else {
                        if (!mIsCollapsible) {
                            updateViewsVisibility = true;
                            mIsCollapsible = true;
                        }
                    }


                    mExpandedContentHeight = newExpandedContentHeight;
                }

                if (mIsCollapsible) {
                    maxWidth = maxWidthCollapsable;
                    mMaxHeaderHeight = maxHeaderHeightCollapsable;
                    mMaxFooterHeight = maxFooterHeightCollapsable;
                } else {
                    maxWidth = maxWidthFixed;
                    mMaxHeaderHeight = maxHeaderHeightFixed;
                    mMaxFooterHeight = maxFooterHeightFixed;
                }

                if (mIsCollapsible) {
                    int intendedCollapsedContentHeight = mCollapsedTotalHeight - maxFooterHeightCollapsable - maxHeaderHeightCollapsable
                            - paddingTop - paddingBottom;
                    if (intendedCollapsedContentHeight < 0) {
                        intendedCollapsedContentHeight = 0;
                    }

                    final int collapsedContentHeightMeasureSpec = getChildMeasureSpec(heightMeasureSpec, paddingTop
                            + paddingBottom + lp.topMargin + lp.bottomMargin, intendedCollapsedContentHeight);
                    collapsableView.measure(childWidthMeasureSpec, collapsedContentHeightMeasureSpec);

                    int newCollapsedContentHeight = collapsableView.getMeasuredHeight();

                    if (newCollapsedContentHeight != mCollapsedContentHeight && mAlignToLines) {
                        newCollapsedContentHeight = correctCollapsedSize(newCollapsedContentHeight, collapsableView);
                    }
                    if (newCollapsedContentHeight != mCollapsedContentHeight) {
                        mCollapsedContentHeight = newCollapsedContentHeight;
                    }

                    mCollapsedTotalHeightToReturn = mCollapsedContentHeight
                            + maxFooterHeightCollapsable
                            + maxHeaderHeightCollapsable
                            + paddingTop + paddingBottom;
                    if (mIsCollapsed) {
                        mSetScrollOnLayout = mCollapsedContentHeight;
                    } else {
                        mSetScrollOnLayout = mExpandedContentHeight;
                    }
                } else {
                    mSetScrollOnLayout = mExpandedContentHeight;
                }

            }

            collapsableView.measure(childWidthMeasureSpec, childHeightMeasureSpec);

            maxWidth = Math.max(maxWidth, collapsableView.getMeasuredWidth() + lp.leftMargin + lp.rightMargin);
            maxHeight = Math.max(maxHeight, collapsableView.getMeasuredHeight() + lp.topMargin + lp.bottomMargin);
        }

        if (updateViewsVisibility) {
            int visibility = mIsCollapsible ? View.VISIBLE : View.GONE;
            //hide/show unneeded headers and footers
            for (int i = 0; i < childCount; i++) {
                final View child = getChildAt(i);

                final LayoutParams lp = (LayoutParams) child.getLayoutParams();
                boolean isHeaderView = isHeaderView(child);
                boolean isFooterView = isFooterView(child);
                if ((isHeaderView || isFooterView) && !lp.alwaysVisible) {
                    child.setVisibility(visibility);
                }
            }


            //make collapsable view clickable or not
            setClickable(mIsClickable && mIsCollapsible);
        }
        maxHeight += paddingTop + paddingBottom + mMaxFooterHeight + mMaxHeaderHeight;
        maxWidth += paddingLeft + paddingRight;

        setMeasuredDimension(maxWidth, maxHeight);
    }

    private int correctCollapsedSize(int startSize, View child) {
        TextView tv = findTextViewToCorrect(child);

        if (tv != null) {
            int h = tv.getMeasuredHeight();

            for (int i = 0; i < tv.getLineCount(); i++) {
                Rect bounds = new Rect();
                tv.getLineBounds(i, bounds);
                if (bounds.bottom > h) {
                    return startSize - (h - bounds.top);
                }
            }
        }

        return startSize;
    }

    private TextView findTextViewToCorrect(View child) {
        if (child instanceof TextView) {
            TextView tv = (TextView) child;
            int h = tv.getMeasuredHeight();
            for (int i = 0; i < tv.getLineCount(); i++) {
                Rect bounds = new Rect();
                tv.getLineBounds(i, bounds);
                if (bounds.bottom > h) {
                    return tv;
                }
            }
        } else if (child instanceof ViewGroup) {
            ViewGroup g = (ViewGroup) child;
            int n = g.getChildCount();
            for (int i = 0; i < n; i++) {
                TextView tv = findTextViewToCorrect(g.getChildAt(i));
                if (tv != null) {
                    return tv;
                }
            }
        }
        return null;
    }

    //*******************************************************************************************
    //* Layout
    //*******************************************************************************************
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        if (mSetScrollOnLayout >= 0 && mScroller.getCurrY() != mSetScrollOnLayout) {
            mScroller.startScroll(0, mSetScrollOnLayout, 0, 0, 0);
            mSetScrollOnLayout = -1;
        }
        mInLayout = true;
        final int paddingLeft = getPaddingLeft();
        final int paddingRight = getPaddingRight();
        final int paddingTop = getPaddingTop();
        final int paddingBottom = getPaddingBottom();

        final int parentLeft = paddingLeft;
        final int parentRight = r - l - paddingRight;
        final int parentTop = paddingTop;
        final int parentBottom = b - t - paddingBottom;

        final int childCount = getChildCount();

        boolean hasCollapsibleView = false;
        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);

            if (child.getVisibility() == GONE) {
                continue;
            }
            boolean isHeaderView = isHeaderView(child);
            boolean isFooterView = isFooterView(child);
            boolean isCollapsibleView = isCollapsibleView(child);
            if (!isFooterView && !isHeaderView && !isCollapsibleView) {
                throw new IllegalStateException("Child " + child + " at index " + i
                        + " does not have a valid layout_gravity - must be Gravity.TOP, "
                        + "Gravity.BOTTOM or Gravity.NO_GRAVITY");
            }

            if (isCollapsibleView) {
                if (hasCollapsibleView) {
                    throw new IllegalStateException("only one child can have layout_gravity equal Gravity.NO_GRAVITY");
                }
                hasCollapsibleView = true;
            }
            final LayoutParams lp = (LayoutParams) child.getLayoutParams();
            int horizontalGravity = getHorizontalGravity(child);
            final int width = child.getMeasuredWidth();
            final int height = child.getMeasuredHeight();

            int childLeft = 0;
            int childRight = 0;
            if (horizontalGravity == Gravity.RIGHT) {
                childRight = parentRight - lp.rightMargin;
                childLeft = childRight - width;
            } else if (horizontalGravity == Gravity.CENTER_HORIZONTAL) {
                childLeft = (parentLeft + lp.leftMargin + parentRight - lp.leftMargin - width) / 2;
                childRight = childLeft + width;
            } else {
                childLeft = parentLeft + lp.leftMargin;
                childRight = childLeft + width;
            }

            if (isCollapsibleView) {
                final int childTop = parentTop + mMaxHeaderHeight + lp.topMargin;
                child.layout(childLeft, childTop, childRight, childTop + height);
            } else if (isHeaderView) {
                final int childTop = parentTop + lp.topMargin;
                child.layout(childLeft, childTop, childRight, childTop + height);
            } else if (isFooterView) {
                final int childBottom = parentBottom - lp.bottomMargin;
                child.layout(childLeft, childBottom - height, childRight, childBottom);
            }
        }
        mInLayout = false;
        if (mIsAnimating) {
            if (mListener != null) {
                int delta = mScroller.getCurrY() - mCurrY;
                mCurrY = mScroller.getCurrY();
                if (mScroller.getFinalY() == mExpandedContentHeight) {
                    mListener.onExpand(CollapsibleLayout.this, delta);
                } else {
                    mListener.onCollapse(CollapsibleLayout.this, delta);
                }
            }
        }
        requestLayoutDelayed();
    }

    //*******************************************************************************************
    //*Helper functions
    //*******************************************************************************************
    int getHorizontalGravity(View child) {
        final int gravity = ((LayoutParams) child.getLayoutParams()).gravity;
        final int absGravity = GravityCompat.getAbsoluteGravity(gravity, ViewCompat.getLayoutDirection(child));
        return absGravity & Gravity.HORIZONTAL_GRAVITY_MASK;
    }

    boolean isCollapsibleView(View child) {
        return ((LayoutParams) child.getLayoutParams()).gravity == Gravity.NO_GRAVITY;
    }

    boolean isFooterView(View child) {
        final int gravity = ((LayoutParams) child.getLayoutParams()).gravity;
        return (gravity & Gravity.BOTTOM) == Gravity.BOTTOM;
    }

    boolean isHeaderView(View child) {
        final int gravity = ((LayoutParams) child.getLayoutParams()).gravity;
        return (gravity & Gravity.TOP) == Gravity.TOP;
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (mIsCollapsed) {
            mergeDrawableStates(drawableState, DRAWABLE_STATE_COLLAPSED);
        }
        return drawableState;
    }

    //*******************************************************************************************
    //*Creating custom Layout Parameters
    //*******************************************************************************************
    @Override
    protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
        return p instanceof LayoutParams && super.checkLayoutParams(p);
    }

    @Override
    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        return p instanceof LayoutParams ? new LayoutParams((LayoutParams) p)
                : p instanceof MarginLayoutParams ? new LayoutParams((MarginLayoutParams) p)
                : new LayoutParams(p);
    }

    @Override
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new LayoutParams(getContext(), attrs);
    }

    @SuppressWarnings("deprecation")
    @Override
    protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
    }

    public static class LayoutParams extends MarginLayoutParams {

        public int gravity = Gravity.NO_GRAVITY;
        public boolean alwaysVisible = false;

        public LayoutParams(Context c, AttributeSet attrs) {
            super(c, attrs);

            final TypedArray a = c.obtainStyledAttributes(attrs, R.styleable.CollapsibleLayout_Layout);
            gravity = a.getInt(R.styleable.CollapsibleLayout_Layout_android_layout_gravity, Gravity.NO_GRAVITY);
            alwaysVisible = a.getBoolean(R.styleable.CollapsibleLayout_Layout_always_visible, false);
            a.recycle();
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }

        public LayoutParams(LayoutParams source) {
            super(source);
            this.gravity = source.gravity;
            this.alwaysVisible = source.alwaysVisible;
        }

        public LayoutParams(ViewGroup.LayoutParams source) {
            super(source);
        }

        public LayoutParams(MarginLayoutParams source) {
            super(source);
        }
    }

    //*******************************************************************************************
    //*Listener interface
    //*******************************************************************************************
    public interface OnCollapseListener {
        void onCollapsed(CollapsibleLayout view);

        void onExpanded(CollapsibleLayout view);

        void onExpand(CollapsibleLayout view, int delta);

        void onCollapse(CollapsibleLayout view, int delta);
    }

    //*******************************************************************************************
    //*State Saving
    //*******************************************************************************************
    static class SavedState extends BaseSavedState {
        boolean isCollapsed;

        /**
         * Constructor called from {@link CompoundButton#onSaveInstanceState()}
         */
        SavedState(Parcelable superState) {
            super(superState);
        }

        /**
         * Constructor called from {@link #CREATOR}
         */
        private SavedState(Parcel in) {
            super(in);
            isCollapsed = (Boolean) in.readValue(null);
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeValue(isCollapsed);
        }

        public static final Creator<SavedState> CREATOR
                = new Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();

        SavedState ss = new SavedState(superState);

        ss.isCollapsed = isCollapsed();
        return ss;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;

        super.onRestoreInstanceState(ss.getSuperState());
        mIsCollapsed = ss.isCollapsed;
        requestLayout();
    }
}