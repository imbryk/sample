package com.imbryk.sample.android;

import android.app.Activity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;

import static com.imbryk.sample.android.RxPausable.terminate;

public abstract class Pilot<VIEW> {


    boolean isCreated = false;
    boolean isFinished = false;

    protected VIEW mView;
    protected Activity mActivityContext;

    private List<RxPausable> mRxPausables = new ArrayList<>();
    private List<Subscription> mSubscriptions = new ArrayList<>();

    protected void setView(VIEW view) {
        mView = view;
    }

    public void onCreate(Bundle savedState) {
    }


    public void onStart(Activity activity) {
        if (!hasView()) {
            throw new RuntimeException("Trying to start a pilot without a view");
        }
        mActivityContext = activity;
        resumeObservers();
    }

    protected boolean hasView() {
        return mView != null;
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
    }

    public void onSaveInstanceState(Bundle outState) {
    }

    public void onStop() {
        pauseObservers();
    }

    public void removeView() {
        mView = null;
        mActivityContext = null;
    }


    public void onFinish() {
        terminateSubscriptions();
    }

    /**
     * @return Returns true if onBackPressed should be consumed
     */
    public boolean onBackButtonPressed() {
        return false;
    }

    protected  <T> PausableSubscription subscribe(Observable<T> observable, Action1<? super T> onNext) {
        RxPausable<T> pausable = toPausable(observable);
        Subscription subscription = pausable.subscribe(onNext);
        manageSubscription(subscription);
        return new PausableSubscription(pausable, subscription);
    }

    protected <T> PausableSubscription subscribe(Observable<T> observable, Action1<? super T> onNext, Action1<Throwable> onError) {
        RxPausable<T> pausable = toPausable(observable);
        Subscription subscription = pausable.subscribe(onNext, onError);
        manageSubscription(subscription);
        return new PausableSubscription(pausable, subscription);
    }

    protected <T> PausableSubscription subscribe(Observable<T> observable, Action1<? super T> onNext, Action1<Throwable> onError, Action0 onCompleted) {
        RxPausable<T> pausable = toPausable(observable);
        Subscription subscription = pausable.subscribe(onNext, onError, onCompleted);
        manageSubscription(subscription);
        return new PausableSubscription(pausable, subscription);
    }

    protected <T> PausableSubscription subscribe(Observable<T> observable, Observer<? super T> observer) {
        RxPausable<T> pausable = toPausable(observable);
        Subscription subscription = pausable.subscribe(observer);
        manageSubscription(subscription);
        return new PausableSubscription(pausable, subscription);
    }

    private void manageSubscription(Subscription subscription) {
        mSubscriptions.add(subscription);
    }

    private <T> RxPausable<T> toPausable(Observable<T> observable) {
        RxPausable<T> rxPausable = RxPausable.from(observable);
        mRxPausables.add(rxPausable);
        return rxPausable;
    }

    protected void unsubscribe(PausableSubscription pausableSubscription) {
        if (pausableSubscription == PausableSubscription.NULL) {
            return;
        }
        Subscription subscription = pausableSubscription.subscription;
        RxPausable pausable = pausableSubscription.pausable;
        mSubscriptions.remove(subscription);
        terminate(subscription);
        mRxPausables.remove(pausable);
        pausable.terminate();
    }

    private void pauseObservers() {
        for (RxPausable rxPausable : mRxPausables) {
            rxPausable.pause();
        }
    }

    private void resumeObservers() {
        for (RxPausable rxPausable : mRxPausables) {
            rxPausable.resume();
        }
    }

    private void terminateSubscriptions() {
        for (RxPausable rxPausable : mRxPausables) {
            rxPausable.terminate();
        }
        for (Subscription subscription : mSubscriptions) {
            terminate(subscription);
        }
    }

    //endregion

    protected static class PausableSubscription {
        public static PausableSubscription NULL = new PausableSubscription(null, null);
        final RxPausable pausable;
        final Subscription subscription;

        PausableSubscription(RxPausable pausable, Subscription subscription) {
            this.pausable = pausable;
            this.subscription = subscription;
        }
    }
}
