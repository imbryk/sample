package com.imbryk.sample.android;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.imbryk.sample.dagger.BaseActivityComponent;
import com.imbryk.sample.dagger.BaseComponentFactory;
import com.imbryk.sample.dagger.DaggerBaseActivityComponent;

import javax.inject.Inject;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    public static final int NO_LAYOUT = 0;

    @Inject
    ControlTower mControlTower;

    private BaseActivityComponent mDaggerComponent;

    protected BaseActivityComponent createComponent() {
        return DaggerBaseActivityComponent.builder()
                .baseComponent(new BaseComponentFactory().get())
                .build();
    }

    /**
     * Implementing activity *must* call the inject(this) on the component to initialize
     * Dependency injection
     *
     * @param component - Dagger component returned previously in createComponent method
     */
    protected void inject(BaseActivityComponent component) {
        component.inject(this);
    }

    private BaseActivityComponent getComponent() {
        if (mDaggerComponent == null) {
            Object nci = getLastCustomNonConfigurationInstance();

            if (nci != null && nci instanceof BaseActivityComponent) {
                mDaggerComponent = (BaseActivityComponent) nci;
            } else {
                mDaggerComponent = createComponent();
            }
        }
        return mDaggerComponent;
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return mDaggerComponent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject(getComponent());
        mControlTower.setSavedInstanceState(savedInstanceState);
        inflate();
    }

    protected void inflate() {
        int layoutResId = getLayoutResource();
        if (layoutResId != NO_LAYOUT) {
            setContentView(layoutResId);
            injectView();
        }
    }


    protected void injectView() {
        ButterKnife.inject(this);
    }


    @Override
    public void onBackPressed() {
        if (!mControlTower.onBackPressed()) {
            super.onBackPressed();
        }
    }

    protected abstract int getLayoutResource();


    @Override
    protected void onStart() {
        super.onStart();
        boardPilots();
        startPilots();
    }


    @Override
    protected void onStop() {
        super.onStop();
        stopPilots();
        removePilotViewsIfNeeded();
        finishPilotsIfNeeded();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removePilotViews();
        if (!isChangingConfigurations()) {
            destroyPilots();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        savePilotStates(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        restorePilotStates(savedInstanceState);
    }

    //region Pilots

    private void boardPilots() {
        setPilotViews();
        if (!mControlTower.isInitiated()) {
            initPilots();
            mControlTower.setInitiated();
        }
        registerPilots();
    }

    protected <VIEW> void setPilotView(Pilot<VIEW> pilot, VIEW view) {
        mControlTower.setView(pilot, view);
    }

    protected abstract void setPilotViews();

    protected abstract void registerPilots();

    protected void initPilots() {
    }

    private void startPilots() {
        mControlTower.onStart(this);
    }

    private void stopPilots() {
        mControlTower.onStop();
    }

    private void removePilotViewsIfNeeded() {
        if (isFinishing() || isChangingConfigurations()) {
            mControlTower.removeViews();
        }
    }

    private void removePilotViews() {
        mControlTower.removeViews();
    }

    private void finishPilotsIfNeeded() {
        if (isFinishing()) {
            mControlTower.onFinish();
        }
    }

    private void destroyPilots() {
        mControlTower.onDestroy();
    }

    private void savePilotStates(Bundle outState) {
        mControlTower.onSaveInstanceState(outState);
    }

    private void restorePilotStates(Bundle savedInstanceState) {
        mControlTower.onRestoreInstanceState(savedInstanceState);
    }

    final protected void registerPilots(Pilot... pilots) {
        for (Pilot p : pilots) {
            mControlTower.registerPilot(p);
        }
    }

    final protected void unregisterPilots(Pilot... pilots) {
        for (Pilot p : pilots) {
            mControlTower.unregisterPilot(p);
        }
    }
    //endregion

}
