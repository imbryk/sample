package com.imbryk.sample.android;

import android.app.Application;

import com.imbryk.sample.dagger.BaseComponentFactory;

public class App extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        new BaseComponentFactory().
                create(this);
    }
}
