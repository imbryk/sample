package com.imbryk.sample.android;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.subjects.ReplaySubject;

public class RxPausable<T> {

    private final Subscription mSubscription;
    private boolean mResumed = true;
    private ReplaySubject<T> mEmittingSubject;
    private ReplaySubject<T> mCachingSubject;
    private Observer<T> mObserver = new Observer<T>() {
        @Override
        public void onCompleted() {
            if (mResumed) {
                mEmittingSubject.onCompleted();
            } else {
                mCachingSubject.onCompleted();
            }
        }

        @Override
        public void onError(Throwable e) {
            if (mResumed) {
                mEmittingSubject.onError(e);
            } else {
                mCachingSubject.onError(e);
            }
        }

        @Override
        public void onNext(T t) {
            if (mResumed) {
                mEmittingSubject.onNext(t);
            } else {
                mCachingSubject.onNext(t);
            }
        }
    };

    public static <T> RxPausable<T> from(Observable<T> observable) {
        return new RxPausable<>(observable);
    }

    RxPausable(Observable<T> observable) {
        mEmittingSubject = ReplaySubject.create();

        mSubscription = observable.subscribe(mObserver);
    }

    public Subscription subscribe(Action1<? super T> onNext) {
        return mEmittingSubject.subscribe(onNext);
    }

    public Subscription subscribe(Action1<? super T> onNext, Action1<Throwable> onError) {
        return mEmittingSubject.subscribe(onNext, onError);
    }

    public Subscription subscribe(Action1<? super T> onNext, Action1<Throwable> onError, Action0 onCompleted) {
        return mEmittingSubject.subscribe(onNext, onError, onCompleted);
    }

    public Subscription subscribe(Observer<? super T> observer) {
        return mEmittingSubject.subscribe(observer);
    }

    public void pause() {
        if (mResumed) {
            mResumed = false;
            mCachingSubject = ReplaySubject.create();
        }
    }

    public void resume() {
        if (!mResumed) {
            mResumed = true;
            mCachingSubject.subscribe(mObserver).unsubscribe();
        }
    }

    public void terminate() {
        terminate(mSubscription);
    }

    public static void terminate(Subscription subscription) {
        if (subscription == null) return;
        if (subscription.isUnsubscribed()) return;

        subscription.unsubscribe();
    }

}
