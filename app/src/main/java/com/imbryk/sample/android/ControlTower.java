package com.imbryk.sample.android;

import android.app.Activity;
import android.os.Bundle;

import com.imbryk.sample.dagger.PerView;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

@PerView
public class ControlTower {

    private Set<Pilot> mPilots = new LinkedHashSet<>();
    private Map<Pilot, Object> mPilotViews = new HashMap<>();
    private Bundle mSavedState;

    private boolean mInitiated;

    @Inject
    public ControlTower() {
        //no-op for injection
    }

    public void registerPilot(Pilot pilot) {
        if (!mPilots.contains(pilot)) {
            mPilots.add(pilot);
        }
        if (!pilot.isCreated) {
            pilot.isCreated = true;
            pilot.onCreate(mSavedState);
        }
    }

    public void unregisterPilot(Pilot pilot) {
        mPilots.remove(pilot);
    }


    public void onStart(Activity activity) {
        for (Pilot pilot : mPilots) {
            pilot.onStart(activity);
        }
    }

    public void onStop() {
        for (Pilot pilot : mPilots) {
            pilot.onStop();
        }
    }

    public <VIEW> void setView(Pilot<VIEW> pilot, VIEW view) {
        if (mPilotViews.get(pilot) != view) {
            pilot.setView(view);
            mPilotViews.put(pilot, view);
        }
    }

    public void removeViews() {
        for (Pilot pilot : mPilots) {
            if (mPilotViews.get(pilot) != null) {
                pilot.removeView();
                mPilotViews.remove(pilot);
            }
        }

    }

    public void onFinish() {
        for (Pilot pilot : mPilots) {
            if (!pilot.isFinished) {
                pilot.onFinish();
                pilot.isFinished = true;
            }
        }
    }

    public void onDestroy() {
        for (Pilot pilot : mPilots) {
            if (!pilot.isFinished) {
                pilot.onFinish();
                pilot.isFinished = true;
            }
        }
        mPilots.clear();

    }

    public void onSaveInstanceState(Bundle outState) {
        for (Pilot pilot : mPilots) {
            pilot.onSaveInstanceState(outState);
        }
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        for (Pilot pilot : mPilots) {
            pilot.onRestoreInstanceState(savedInstanceState);
        }
    }

    void setSavedInstanceState(Bundle savedInstanceState) {
        mSavedState = savedInstanceState;
    }

    /**
     * Pass onBackPressed to pilots
     *
     * @return Returns true if any of the pilots has consumed the event
     */
    public boolean onBackPressed() {
        for (Pilot pilot : mPilots) {
            if (pilot.onBackButtonPressed()) {
                return true;
            }
        }
        return false;
    }

    public void setInitiated() {
        mInitiated = true;
    }

    public boolean isInitiated() {
        return mInitiated;
    }

}