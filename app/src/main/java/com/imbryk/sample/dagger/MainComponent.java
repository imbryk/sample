package com.imbryk.sample.dagger;

import com.imbryk.sample.app.MainActivity;

import dagger.Component;

@PerView
@Component(
        dependencies = BaseComponent.class
)
public interface MainComponent extends BaseActivityComponent{
    void inject(MainActivity activity);
}
