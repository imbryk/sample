package com.imbryk.sample.dagger;

import com.imbryk.sample.android.BaseActivity;

import dagger.Component;

@PerView
@Component(
        dependencies = {
                BaseComponent.class
        }
)
public interface BaseActivityComponent {
    void inject(BaseActivity activity);
}
