package com.imbryk.sample.dagger;


import com.imbryk.sample.android.App;

public class BaseComponentFactory {
    private static BaseComponent sBaseComponent;

    public BaseComponent create(App app) {
        if (sBaseComponent == null) {
            sBaseComponent = DaggerBaseComponent.builder()
                    .appModule(new AppModule(app.getApplicationContext()))
                    .build();
        }
        return sBaseComponent;
    }
    public BaseComponent get() {
        if (sBaseComponent == null) {
            throw new RuntimeException("call create from application before calling get()");
        }
        return sBaseComponent;
    }
}
