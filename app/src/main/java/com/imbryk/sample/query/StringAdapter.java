package com.imbryk.sample.query;

import java.util.Set;

class StringAdapter {
    static String parse(Query query) {
        StringBuilder builder = new StringBuilder();
        feedTopLevelParams(builder, query.getParams());
        return builder.toString().trim();
    }

    private static void feedTopLevelParams(StringBuilder builder, QueryParam.ArrayParam params) {
        Set<String> keySet = params.getKeys();
        for (String key : keySet) {
            feed(builder, key, params.get(key));
        }
    }

    private static void feed(StringBuilder builder, String key, QueryParam param) {
        if (param instanceof QueryParam.StringParam) {
            addParam(builder, key, param.asString());
        } else if (param instanceof QueryParam.NumberParam) {
            addParam(builder, key, getNumberValue((QueryParam.NumberParam) param));
        } else if (param instanceof QueryParam.ArrayParam) {
            QueryParam.ArrayParam arrayParam = (QueryParam.ArrayParam) param;
            feedArray(builder, key, arrayParam);
        }
    }

    private static void addParam(StringBuilder builder, String key, String value) {
        if (builder.length() > 0) {
            builder.append("&");
        }
        builder
                .append(key)
                .append("=")
                .append(value);
    }

    private static String getNumberValue(QueryParam.NumberParam param) {
        float floatValue = param.asFloat();
        if (isInt(floatValue)) {
            return String.valueOf((int) floatValue);
        } else {
            return String.valueOf(floatValue);
        }
    }

    private static boolean isInt(float floatValue) {
        return (int) floatValue == floatValue;
    }

    private static void feedArray(StringBuilder builder, String keyBase, QueryParam.ArrayParam arrayParam) {
        String arrayKey = keyBase + "[%s]";

        for (int i = 0; i < arrayParam.getSize(); i++) {
            String formattedKey = String.format(arrayKey, i);
            feed(builder, formattedKey, arrayParam.get(i));
        }

        Set<String> keySet = arrayParam.getKeys();
        for (String innerKey : keySet) {
            String formattedKey = String.format(arrayKey, innerKey);
            feed(builder, formattedKey, arrayParam.get(innerKey));
        }
    }


}
