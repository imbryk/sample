package com.imbryk.sample.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class QueryParam {
    public static QueryParam NULL = new QueryParam() {
    };

    static QueryParam createPrimitive(String rawValue) {
        if (isNumber(rawValue)) {
            return new NumberParam(Float.valueOf(rawValue));
        }
        return new StringParam(rawValue);
    }

    static QueryParam createNumber(Number value) {
        return new NumberParam(value.floatValue());
    }

    private static boolean isNumber(String value) {
        if (value == null) {
            return false;
        }
        char[] data = value.toCharArray();
        int length = data.length;
        if (length <= 0) {
            return false;
        }
        int index = 0;
        if (data[0] == '-' && length > 1) {
            index = 1;
        }
        for (; index < length; index++) {
            if (data[index] != '.' && (data[index] < '0' || data[index] > '9')) {
                return false;
            }
        }
        return true;
    }

    private static boolean isIndex(String value) {
        if (value == null) {
            return false;
        }
        char[] data = value.toCharArray();
        int length = data.length;
        if (length <= 0) {
            return false;
        }

        for (int index = 0; index < length; index++) {
            if (data[index] < '0' || data[index] > '9') {
                return false;
            }
        }
        return true;
    }

    public String asString() {
        throw new ClassCastException("Parameter is not a string");
    }

    public int asInt() {
        throw new ClassCastException("Parameter is not a number");
    }

    public float asFloat() {
        throw new ClassCastException("Parameter is not a number");
    }

    public static class StringParam extends QueryParam {
        private final String stringValue;

        @Override
        public String asString() {
            return stringValue;
        }

        private StringParam(String value) {
            stringValue = value;
        }
    }

    public static class NumberParam extends QueryParam {
        private final int intValue;
        private final float floatValue;

        public boolean isInteger() {
            return intValue == (int) floatValue;
        }

        @Override
        public String asString() {
            if (isInteger()) {
                return String.valueOf(intValue);
            } else {
                return String.valueOf(floatValue);
            }
        }

        @Override
        public int asInt() {
            return intValue;
        }

        @Override
        public float asFloat() {
            return floatValue;
        }

        NumberParam(float value) {
            intValue = (int) value;
            floatValue = value;
        }
    }

    static QueryParam merge(QueryParam previous, QueryParam next) {
        if (previous instanceof ArrayParam && next instanceof ArrayParam) {
            return new ArrayParam((ArrayParam) previous, (ArrayParam) next);
        } else {
            //fallback in case of "a=1&a[1]=2" or "a=1&a=2"
            return previous;
        }
    }

    public static class ArrayParam extends QueryParam {

        private Map<String, QueryParam> map = new LinkedHashMap<>();
        private List<QueryParam> list = new ArrayList<>();

        ArrayParam() {
        }

        ArrayParam(ArrayParam previous, ArrayParam next) {
            mergeLists(previous, next);
            mergeMaps(previous, next);
        }

        private void mergeLists(ArrayParam previous, ArrayParam next) {
            int previousSize = previous.list.size();
            int nextSize = next.list.size();
            int maxSize = previousSize > nextSize ? previousSize : nextSize;
            for (int i = 0; i < maxSize; i++) {
                QueryParam previousItem = i < previousSize ? previous.list.get(i) : null;
                QueryParam nextItem = i < nextSize ? next.list.get(i) : null;
                QueryParam item = null;
                if (previousItem != null && nextItem != null) {
                    item = merge(previousItem, nextItem);
                } else if (previousItem != null) {
                    item = previousItem;
                } else if (nextItem != null) {
                    item = nextItem;
                }
                list.add(i, item);
            }
        }

        private void mergeMaps(ArrayParam previous, ArrayParam next) {
            map = new HashMap<>(previous.map);
            Set<String> nextKeyset = next.map.keySet();
            for (String key : nextKeyset) {
                QueryParam nextItem = next.map.get(key);
                if (map.containsKey(key)) {
                    QueryParam previousItem = map.get(key);
                    map.put(key, merge(previousItem, nextItem));
                } else {
                    map.put(key, nextItem);
                }
            }
        }

        public QueryParam get(int position) {
            return list.get(position);
        }

        public ArrayParam getArray(int position) {
            return (ArrayParam) list.get(position);
        }

        public QueryParam get(String key) {
            return map.get(key);
        }

        public ArrayParam getArray(String key) {
            return (ArrayParam) map.get(key);
        }

        void put(String key, QueryParam value) {
            if (key.isEmpty()) {
                put(value);
            } else if (isIndex(key)) {
                put(Integer.valueOf(key), value);
            } else {
                map.put(key, value);
            }
        }

        void put(QueryParam value) {
            list.add(value);
        }

        void put(int index, QueryParam value) {
            while (list.size() < index) {
                list.add(null);
            }
            list.add(index, value);
        }

        public boolean containsKey(String key) {
            return map.containsKey(key);
        }

        public int getSize() {
            return list.size();
        }

        public Set<String> getKeys() {
            return map.keySet();
        }
    }
}