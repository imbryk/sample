package com.imbryk.sample.query;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

class ClassAdapter {
    static <T> T parse(Query query, Class<T> classOfT) throws IllegalAccessException, InstantiationException {
        T instance = classOfT.newInstance();
        QueryParam.ArrayParam params = query.getParams();
        feed(instance, params, classOfT);
        return instance;
    }

    private static void feed(Object instance, QueryParam.ArrayParam params, Class<?> classOfT) throws IllegalAccessException, InstantiationException {
        Field[] fields = classOfT.getDeclaredFields();
        for (Field field : fields) {
            QueryName nameAnnotation = field.getAnnotation(QueryName.class);
            String queryName = nameAnnotation != null ? nameAnnotation.value() : field.getName();
            QueryParam rawValue = params.get(queryName);
            setField(field, instance, rawValue);
        }
    }

    private static void feedList(List<Object> list, QueryParam.ArrayParam rawValue, Class<?> type) throws IllegalAccessException, InstantiationException {
        int size = rawValue.getSize();
        for (int i = 0; i < size; i++) {
            Object value = getValue(type, null, rawValue.get(i));
            list.add(i, value);
        }
    }

    private static void setField(Field field, Object instance, QueryParam rawValue) throws IllegalAccessException, InstantiationException {
        if (rawValue == null) {
            return;
        }
        field.setAccessible(true);
        Class<?> type = field.getType();
        Class<?> listClass = null;
        if (field.getGenericType() instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
            listClass = (Class<?>) parameterizedType.getActualTypeArguments()[0];
        }
        Object value = getValue(type, listClass, rawValue);
        field.set(instance, value);
    }

    private static Object getValue(Class<?> type, Class<?> listClass, QueryParam rawValue) {
        try {
            if (isInt(type)) {
                return rawValue.asInt();
            } else if (isFloat(type)) {
                return rawValue.asFloat();
            } else if (isString(type)) {
                return rawValue.asString();
            } else if (isList(type) && rawValue instanceof QueryParam.ArrayParam) {
                List<Object> list = new ArrayList<>();

                feedList(list, (QueryParam.ArrayParam) rawValue, listClass);

                return list;
            } else if (rawValue instanceof QueryParam.ArrayParam) {
                Object innerInstance = type.newInstance();
                feed(innerInstance, (QueryParam.ArrayParam) rawValue, type);
                return innerInstance;
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static boolean isList(Class<?> type) {
        return List.class.isAssignableFrom(type);
    }

    private static boolean isString(Class<?> type) {
        return String.class.isAssignableFrom(type);
    }

    private static boolean isFloat(Class<?> type) {
        return Float.class.isAssignableFrom(type) || float.class.isAssignableFrom(type);
    }

    private static boolean isInt(Class<?> type) {
        return Integer.class.isAssignableFrom(type) || int.class.isAssignableFrom(type);
    }
}
