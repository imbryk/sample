package com.imbryk.sample.query;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

class Parser {
    private final String queryString;
    private final QueryParam.ArrayParam params;
    private final String charset;

    Parser(String queryString, String charset) {
        this.queryString = queryString;
        this.charset = charset;
        this.params = new QueryParam.ArrayParam();
    }

    QueryParam.ArrayParam parseString() {
        String[] parts = queryString.split("&");
        for (String part : parts) {
            parsePart(part);
        }
        return params;
    }

    private void parsePart(String part) {
        try {
            String[] pair = part.split("=");
            String key = URLDecoder.decode(pair[0], charset);
            String value = pair.length > 1 ?
                    URLDecoder.decode(pair[1], charset) :
                    null;
            addParam(key, value);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void addParam(String key, String value) {
        if (isArray(key)) {
            parseArray(key, QueryParam.createPrimitive(value));
        } else {
            addParam(key, QueryParam.createPrimitive(value));
        }
    }

    private boolean isArray(String key) {
        return key.contains("[") && key.lastIndexOf("]") == key.length() - 1;
    }

    private void parseArray(String key, QueryParam value) {
        String head = getHead(key);
        String tail = getTail(key);
        if (head != null && tail != null) {
            QueryParam.ArrayParam param = new QueryParam.ArrayParam();
            param.put(tail, value);
            parseArray(head, param);
        } else {
            addParam(key, value);
        }

    }

    private String getHead(String key) {
        int index = key.lastIndexOf("[");
        if (index < 0) {
            return null;
        } else {
            return key.substring(0, index);
        }
    }

    private String getTail(String key) {
        int frontIndex = key.lastIndexOf("[");
        int backIndex = key.lastIndexOf("]");
        if (frontIndex < 0 || backIndex < frontIndex) {
            return null;
        } else {
            return key.substring(key.lastIndexOf("[") + 1, key.lastIndexOf("]"));
        }
    }

    private void addParam(String key, QueryParam value) {
        if (params.containsKey(key)) {
            QueryParam previous = params.get(key);
            params.put(key, QueryParam.merge(previous, value));
        } else {
            params.put(key, value);
        }
    }

}
