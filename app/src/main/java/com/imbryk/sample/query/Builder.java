package com.imbryk.sample.query;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


class Builder {
    private final Object input;
    private final Class classOfInput;
    private final QueryParam.ArrayParam params;

    Builder(Object input) {
        this.input = input;
        this.classOfInput = input.getClass();
        this.params = new QueryParam.ArrayParam();
    }

    QueryParam.ArrayParam build() {
        Field[] fields = classOfInput.getDeclaredFields();
        for (Field field : fields) {
            parseField(field);
        }
        return params;
    }

    private void parseField(Field field) {
        QueryName nameAnnotation = field.getAnnotation(QueryName.class);
        String queryName = nameAnnotation != null ? nameAnnotation.value() : field.getName();

        try {
            QueryParam param = parseObject(field.get(input));
            if (param != null) {
                addParam(queryName, param);
            }
        } catch (IllegalAccessException e) {
            //do not read inaccessible fields
        }
    }

    private QueryParam parseObject(Object object) {
        if (object == null) {
            return null;
        } else if (isNumber(object)) {
            return QueryParam.createNumber((Number) object);
        } else if (isString(object)) {
            return QueryParam.createPrimitive(object.toString());
        } else if (isCollection(object)) {
            return parseCollection(object);
        } else if (isMap(object)) {
            return parseMap(object);
        } else if (isArray(object)) {
            return parseArray(object);
        } else {
            return parseComplex(object);
        }
    }

    private boolean isNumber(Object object) {
        Class<?> type = object.getClass();
        return Number.class.isAssignableFrom(type)
                || byte.class.isAssignableFrom(type)
                || int.class.isAssignableFrom(type)
                || short.class.isAssignableFrom(type)
                || long.class.isAssignableFrom(type)
                || float.class.isAssignableFrom(type)
                || double.class.isAssignableFrom(type);
    }

    private boolean isString(Object object) {
        Class<?> type = object.getClass();
        return CharSequence.class.isAssignableFrom(type);
    }

    private boolean isCollection(Object object) {
        Class<?> type = object.getClass();
        return Collection.class.isAssignableFrom(type);
    }

    private boolean isMap(Object object) {
        Class<?> type = object.getClass();
        return Map.class.isAssignableFrom(type);
    }

    private boolean isArray(Object object) {
        Class<?> type = object.getClass();
        return type.isArray();
    }

    private QueryParam.ArrayParam parseCollection(Object object) {
        QueryParam.ArrayParam arrayParam = new QueryParam.ArrayParam();
        Collection<?> collection = (Collection<?>) object;
        Iterator<?> iterator = collection.iterator();
        while (iterator.hasNext()) {
            Object value = iterator.next();
            arrayParam.put(parseObject(value));
        }
        return arrayParam;
    }

    private QueryParam.ArrayParam parseMap(Object object) {
        QueryParam.ArrayParam arrayParam = new QueryParam.ArrayParam();
        Map map = (Map) object;
        Set<?> keySet = map.keySet();
        for (Object key : keySet) {
            Object value = map.get(key);
            arrayParam.put(key.toString(), parseObject(value));
        }
        return arrayParam;
    }

    private QueryParam parseArray(Object object) {
        QueryParam.ArrayParam arrayParam = new QueryParam.ArrayParam();
        int length = Array.getLength(object);
        for (int i = 0; i < length; i++) {
            Object value = Array.get(object, i);
            arrayParam.put(i, parseObject(value));
        }
        return arrayParam;
    }

    private QueryParam.ArrayParam parseComplex(Object object) {
        Builder builder = new Builder(object);
        return builder.build();
    }

    private void addParam(String key, QueryParam value) {
        params.put(key, value);
    }
}
