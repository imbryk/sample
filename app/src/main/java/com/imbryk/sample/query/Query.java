package com.imbryk.sample.query;


public class Query {
    private static final String DEFAULT_CHARSET = "UTF-8";

    private QueryParam.ArrayParam params;
    private String queryString;

    Query(QueryParam.ArrayParam params) {
        this.params = params;
    }

    public static Query build(Object input) throws IllegalAccessException {
        Builder builder = new Builder(input);
        return new Query(builder.build());
    }

    public static Query fromString(String queryString) {
        Parser parser = new Parser(queryString, DEFAULT_CHARSET);
        Query query = new Query(parser.parseString());
        query.queryString = queryString;
        return query;
    }

    public QueryParam get(String key) {
        if (params.containsKey(key)) {
            return params.get(key);
        } else {
            return QueryParam.NULL;
        }
    }

    public String getString(String key) {
        QueryParam param = params.get(key);
        return param.asString();
    }

    public int getInt(String key) {
        QueryParam param = params.get(key);
        return param.asInt();
    }

    public float getFloat(String key) {
        QueryParam param = params.get(key);
        return param.asFloat();
    }

    public QueryParam.ArrayParam getArray(String key) {
        QueryParam param = params.get(key);
        return (QueryParam.ArrayParam) param;
    }

    public QueryParam.ArrayParam getParams() {
        return params;
    }

    public <T> T toClass(Class<T> classOfT) {
        try {
            return ClassAdapter.parse(this, classOfT);

        } catch (IllegalAccessException e) {
            return null;
        } catch (InstantiationException e) {
            return null;
        }
    }

    public String toQueryString() {
        if (queryString == null) {
            queryString = StringAdapter.parse(this);
        }
        return queryString;
    }
}
