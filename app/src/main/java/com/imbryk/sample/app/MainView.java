package com.imbryk.sample.app;

public interface MainView {
    void setData(CharSequence text);
    void displayProgress();

    void setListener(ActionListener listener);

    interface ActionListener  {
        void onRequestData();
    }
}
