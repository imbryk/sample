package com.imbryk.sample.app;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.TextView;

import com.imbryk.sample.R;
import com.imbryk.sample.widget.CollapsibleLayout;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

class MainViewImpl implements MainView {

    public static final String STATE_BASE = MainViewImpl.class.getCanonicalName();
    public static final String STATE_FAB_VISIBILITY = STATE_BASE + ".STATE_FAB_VISIBILITY";
    public static final String STATE_TEXT = STATE_BASE + ".STATE_TEXT";
    @InjectView(R.id.fab)
    FloatingActionButton mFab;

    @InjectView(R.id.text)
    TextView mText;

    @InjectView(R.id.collapsible)
    CollapsibleLayout mCollapsibleLayout;

    @InjectView(R.id.collapsible_toggle)
    TextView mToggle;

    private ActionListener mListener = () -> {};

    public MainViewImpl(View root) {
        ButterKnife.inject(this, root);
        init();
    }

    private void init() {
        mCollapsibleLayout.setOnCollapseListener(new CollapsibleLayout.OnCollapseListener() {
            @Override
            public void onCollapsed(CollapsibleLayout view) {
            }

            @Override
            public void onExpanded(CollapsibleLayout view) {
            }

            @Override
            public void onExpand(CollapsibleLayout view, int delta) {
                mToggle.setText("less");
            }

            @Override
            public void onCollapse(CollapsibleLayout view, int delta) {
                mToggle.setText("more");
            }
        });
    }

    @Override
    public void setData(CharSequence text) {
        mText.setText("data received:\n"+text);
    }

    @Override
    public void displayProgress() {
        mText.setText("loading data - feel free to rotate the device");
        mFab.setVisibility(View.GONE);
    }

    @Override
    public void setListener(ActionListener listener) {
        mListener = listener;
    }

    @OnClick(R.id.fab)
    void fabClick() {
        mListener.onRequestData();
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(STATE_BASE, true);
        outState.putInt(STATE_FAB_VISIBILITY, mFab.getVisibility());
        outState.putCharSequence(STATE_TEXT, mText.getText());
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (hasSavedState(savedInstanceState)) {
            //noinspection WrongConstant
            mFab.setVisibility(savedInstanceState.getInt(STATE_FAB_VISIBILITY));
            mText.setText(savedInstanceState.getString(STATE_TEXT));
        }
    }

    private boolean hasSavedState(Bundle savedInstanceState) {
        return savedInstanceState.getBoolean(STATE_BASE, false);
    }
}
