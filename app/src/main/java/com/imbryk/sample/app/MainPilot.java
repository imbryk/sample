package com.imbryk.sample.app;

import android.app.Activity;

import com.imbryk.sample.android.Pilot;
import com.imbryk.sample.dagger.PerView;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


@PerView
public class MainPilot extends Pilot<MainView> {

    @Inject
    MainModel mModel;

    @Inject
    public MainPilot() {
    }

    @Override
    public void onStart(Activity activity) {
        super.onStart(activity);
        mView.setListener(this::requestData);
    }

    private void requestData() {
        mView.displayProgress();
        subscribe(prepareObservable(mModel.requestData()),this::setData);
    }

    private <T> Observable<T> prepareObservable(Observable<T> observable) {
        return observable.subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread());
    }

    private void setData(MainModel.ModelData modelData) {
        mView.setData(modelData.data);
    }
}
