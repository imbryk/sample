package com.imbryk.sample.app;

import com.imbryk.sample.dagger.PerView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;

import static rx.Observable.defer;
import static rx.Observable.just;


@PerView
public class MainModel {
    private static final CharSequence LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta, eros vel ulrices congue, tellus urna aliquam velit, non faucibus velit libero et odio. Phasellus dictum lectus lorem, quis vulputate tellus scelerisque in. Sed lobortis sem nec libero bibendum, et congue ex sagittis. Aliquam laoreet mi et tincidunt finibus. Donec sed interdum arcu. Sed ut dolor sapien. In placerat felis sit amet finibus sagittis. Praesent ut lorem iaculis, bibendum dolor id, mollis tellus. Nam ullamcorper massa ac elit dapibus, sit amet dapibus diam mollis. In sed arcu a odio accumsan porta a tempus nisi\n\nPellentesque efficitur sem velit, at fringilla metus fringilla gravida. Nullam ut elit massa. Etiam elementum diam ex, vel vehicula dolor feugiat ac. In ut ex nec diam egestas pretium. Maecenas tincidunt semper dui, quis rutrum mi pellentesque malesuada. Vestibulum ex ante, porttitor in lorem et, hendrerit tincidunt nisl. Nam vitae eleifend mi, sed gravida libero. Nunc dignissim mattis turpis ac posuere. Nullam sodales condimentum tempor. Pellentesque ac justo ac ex cursus pharetra vitae eget ante. Proin sed magna eu diam dapibus porta quis sollicitudin massa";

    @Inject
    public MainModel() {
    }

    Observable<ModelData> requestData() {
        return defer(
                () -> just(new ModelData(LOREM_IPSUM))
        ).delay(5, TimeUnit.SECONDS);
    }

    static class ModelData {
        public final CharSequence data;

        public ModelData(CharSequence data) {
            this.data = data;
        }
    }

}
