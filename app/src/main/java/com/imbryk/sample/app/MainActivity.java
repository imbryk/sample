package com.imbryk.sample.app;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.imbryk.sample.R;
import com.imbryk.sample.android.BaseActivity;
import com.imbryk.sample.dagger.BaseActivityComponent;
import com.imbryk.sample.dagger.BaseComponentFactory;
import com.imbryk.sample.dagger.DaggerBaseActivityComponent;
import com.imbryk.sample.dagger.DaggerMainComponent;
import com.imbryk.sample.dagger.MainComponent;

import javax.inject.Inject;

import butterknife.InjectView;

public class MainActivity extends BaseActivity {

    @InjectView(R.id.root)
    View mRoot;

    @Inject
    MainPilot mPilot;

    MainViewImpl mView;

    @Override
    protected BaseActivityComponent createComponent() {
        return DaggerMainComponent.builder()
                .baseComponent(new BaseComponentFactory().get())
                .build();
    }

    @Override
    protected void inject(BaseActivityComponent component) {
        ((MainComponent)component).inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
        createViewImpl();
    }

    private void createViewImpl() {
        mView = new MainViewImpl(mRoot);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void setPilotViews() {
        setPilotView(mPilot, mView);
    }

    @Override
    protected void registerPilots() {
        registerPilots(mPilot);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mView.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mView.onRestoreInstanceState(savedInstanceState);
    }
}
