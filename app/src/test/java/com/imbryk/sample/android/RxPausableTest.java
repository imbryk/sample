package com.imbryk.sample.android;

import org.junit.Before;
import org.junit.Test;

import rx.observers.TestSubscriber;
import rx.subjects.PublishSubject;


public class RxPausableTest {

    private RxPausable<Integer> systemUnderTest;

    private PublishSubject<Integer> publisher;
    private TestSubscriber<Integer> subscriber;

    @Before
    public void setUp() throws Exception {
        subscriber = TestSubscriber.create();

        publisher = PublishSubject.create();
        systemUnderTest = RxPausable.from(publisher);
    }

    @Test
    public void shouldReceiveCallbacksSentAfterSubscription() throws Exception {
        systemUnderTest.subscribe(subscriber);

        publisher.onNext(1);
        publisher.onNext(2);
        publisher.onNext(3);
        publisher.onCompleted();

        subscriber.assertValues(1, 2, 3);
        subscriber.assertCompleted();
        subscriber.assertValueCount(3);
    }

    @Test
    public void shouldReceiveCallbacksSentBeforeSubscription() throws Exception {
        publisher.onNext(1);
        publisher.onNext(2);
        publisher.onNext(3);
        publisher.onCompleted();

        systemUnderTest.subscribe(subscriber);


        subscriber.assertValues(1, 2, 3);
        subscriber.assertCompleted();
        subscriber.assertValueCount(3);
    }

    @Test
    public void shouldReceiveCallbacksSentBeforeAndAfterSubscription() throws Exception {
        publisher.onNext(1);
        publisher.onNext(2);

        systemUnderTest.subscribe(subscriber);

        publisher.onNext(3);
        publisher.onCompleted();

        subscriber.assertValues(1, 2, 3);
        subscriber.assertCompleted();
        subscriber.assertValueCount(3);
    }

    @Test
    public void shouldEnablePause() throws Exception {
        publisher.onNext(1);
        publisher.onNext(2);

        systemUnderTest.subscribe(subscriber);
        systemUnderTest.pause();

        publisher.onNext(3);
        publisher.onCompleted();

        subscriber.assertValues(1, 2);
        subscriber.assertNotCompleted();
        subscriber.assertValueCount(2);
    }

    @Test
    public void shouldEnableResume() throws Exception {
        publisher.onNext(1);
        publisher.onNext(2);

        systemUnderTest.subscribe(subscriber);
        systemUnderTest.pause();
        systemUnderTest.resume();

        publisher.onNext(3);
        publisher.onCompleted();


        subscriber.assertValues(1, 2, 3);
        subscriber.assertCompleted();
        subscriber.assertValueCount(3);
    }

    @Test
    public void shouldReceiveCallbacksSentDuringPause() throws Exception {
        publisher.onNext(1);
        publisher.onNext(2);

        systemUnderTest.subscribe(subscriber);

        subscriber.assertValues(1, 2);
        subscriber.assertNotCompleted();
        subscriber.assertValueCount(2);

        systemUnderTest.pause();

        publisher.onNext(3);
        publisher.onCompleted();

        systemUnderTest.resume();

        subscriber.assertValues(1, 2, 3);
        subscriber.assertCompleted();
        subscriber.assertValueCount(3);
    }

    @Test
    public void shouldReceiveErrorSentAfterSubscription() throws Exception {
        systemUnderTest.subscribe(subscriber);

        publisher.onError(new Exception());

        subscriber.assertError(Exception.class);
    }

    @Test
    public void shouldReceiveErrorSentBeforeSubscription() throws Exception {
        publisher.onError(new Exception());

        systemUnderTest.subscribe(subscriber);

        subscriber.assertError(Exception.class);
    }

    @Test
    public void shouldReceiveErrorSentDuringPause() throws Exception {
        systemUnderTest.subscribe(subscriber);

        systemUnderTest.pause();
        publisher.onError(new Exception());
        systemUnderTest.resume();

        subscriber.assertError(Exception.class);
    }
}