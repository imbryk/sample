package com.imbryk.sample.android;

import android.app.Activity;

import org.junit.Before;
import org.junit.Test;

import rx.Observable;
import rx.observers.TestSubscriber;
import rx.subjects.PublishSubject;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class PilotTest {

    private Pilot<TestView> systemUnderTest = new TestPilot();

    private TestSubscriber<Integer> subscriber = TestSubscriber.create();


    @Before
    public void setUp() throws Exception {
        systemUnderTest.onCreate(null);
        systemUnderTest.setView(mock(TestView.class));
        systemUnderTest.onStart(mock(Activity.class));

    }

    @Test
    public void shouldSubscribeObservable() throws Exception {
        Observable<Integer> observable = Observable.just(1);

        systemUnderTest.subscribe(observable, subscriber);

        subscriber.assertValueCount(1);
        subscriber.assertValues(1);
        subscriber.assertCompleted();
        subscriber.assertUnsubscribed();
    }

    @Test
    public void shouldNotReceiveCallsWhenStopped() throws Exception {
        PublishSubject<Integer> publisher = PublishSubject.create();
        systemUnderTest.subscribe(publisher, subscriber);

        systemUnderTest.onStop();

        publisher.onNext(1);
        publisher.onCompleted();

        subscriber.assertValueCount(0);
        subscriber.assertNotCompleted();
        assertThat(subscriber.isUnsubscribed()).isFalse();

        systemUnderTest.onStart(mock(Activity.class));

        subscriber.assertValueCount(1);
        subscriber.assertValues(1);
        subscriber.assertCompleted();
        subscriber.assertUnsubscribed();
    }

    @Test
    public void shouldUnsubscribeWhenPilotFinishes() throws Exception {
        PublishSubject<Integer> publisher = PublishSubject.create();
        systemUnderTest.subscribe(publisher, subscriber);

        systemUnderTest.onStop();
        systemUnderTest.onFinish();
        subscriber.assertUnsubscribed();
    }

    private static class TestView {
    }

    private static class TestPilot extends Pilot<TestView> {
    }
}