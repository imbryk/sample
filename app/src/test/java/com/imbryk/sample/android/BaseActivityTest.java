package com.imbryk.sample.android;

import android.os.Bundle;

import com.imbryk.sample.R;
import com.imbryk.sample.dagger.BaseActivityComponent;
import com.imbryk.sample.dagger.BaseMockComponent;
import com.imbryk.sample.dagger.DaggerBaseMockComponent;
import com.imbryk.sample.dagger.PerView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.ShadowActivity;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.internal.ShadowExtractor;

import javax.inject.Inject;

import dagger.Component;
import dagger.Module;
import dagger.Provides;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(
        constants = com.imbryk.sample.BuildConfig.class,
        manifest = "./AndroidManifest.xml",
        sdk = 21,
        shadows = {ShadowActivity.class}

)
public class BaseActivityTest {
    private static MockComponent sComponent;

    @Inject
    DummyPilot mPilot;

    private ActivityController<TestBaseActivity> mActivityCtrl;
    private TestBaseActivity mActivity;

    @Before
    public void setUp() throws Exception {
        sComponent =
                DaggerBaseActivityTest_MockComponent
                        .builder()
                        .baseMockComponent(DaggerBaseMockComponent.create())
                        .build();
        sComponent.inject(this);
        mActivityCtrl = Robolectric.buildActivity(TestBaseActivity.class);
        mActivity = mActivityCtrl.get();
    }

    @Test
    public void pilotShouldBeKeptAfterConfigurationChange() throws Exception {
        launchActivity();

        TestBaseActivity oldActivity = mActivity;
        DummyPilot oldPilot = mActivity.mPilot;
        DummyView oldView = mActivity.mView;

        changeConfiguration();

        TestBaseActivity newActivity = mActivity;
        DummyPilot newPilot = mActivity.mPilot;
        DummyView newView = mActivity.mView;

        assertThat(newActivity).isNotSameAs(oldActivity);
        assertThat(newView).isNotSameAs(oldView);
        assertThat(newPilot).isSameAs(oldPilot);
    }

    //region pilot Lifecycle
    @Test
    public void shouldCallPilotLifecycleMethods() throws Exception {
        InOrder inOrder = inOrder(mPilot);

        reset(mPilot);

        launchActivity();

        inOrder.verify(mPilot).setView(mActivity.mView);
        inOrder.verify(mPilot).initDummy();
        inOrder.verify(mPilot).onCreate(null);
        inOrder.verify(mPilot).onStart(mActivity);
        verifyNoMoreInteractions(mPilot);

        reset(mPilot);

        finishActivity();

        inOrder.verify(mPilot).onStop();
        inOrder.verify(mPilot).removeView();
        inOrder.verify(mPilot).onFinish();
        verifyNoMoreInteractions(mPilot);
    }

    @Test
    public void shouldCallPilotLifecycleMethodsOnConfigurationChange() throws Exception {
        InOrder inOrder = inOrder(mPilot);
        ArgumentCaptor<Bundle> argumentCaptor = ArgumentCaptor.forClass(Bundle.class);

        launchActivity();

        reset(mPilot);

        changeConfiguration();

        inOrder.verify(mPilot).onSaveInstanceState(argumentCaptor.capture());
        inOrder.verify(mPilot).onStop();
        inOrder.verify(mPilot).removeView();

        inOrder.verify(mPilot).setView(mActivity.mView);
        inOrder.verify(mPilot).onStart(mActivity);
        inOrder.verify(mPilot).onRestoreInstanceState(argumentCaptor.getValue());
        verifyNoMoreInteractions(mPilot);
    }

    @Test
    public void shouldCallPilotLifecycleMethodsOnMovedToAndFromBackStack() throws Exception {
        InOrder inOrder = inOrder(mPilot);
        ArgumentCaptor<Bundle> argumentCaptor = ArgumentCaptor.forClass(Bundle.class);

        launchActivity();

        reset(mPilot);

        moveToAndFromBackStack();

        inOrder.verify(mPilot).onSaveInstanceState(argumentCaptor.capture());
        inOrder.verify(mPilot).onStop();

        inOrder.verify(mPilot).onStart(mActivity);
        inOrder.verify(mPilot).onRestoreInstanceState(argumentCaptor.getValue());
        verifyNoMoreInteractions(mPilot);
    }

    @Test
    public void shouldFinishPilotWhenDestroyingActivityInBackStack() throws Exception {
        InOrder inOrder = inOrder(mPilot);

        launchActivity();

        reset(mPilot);

        moveActivityToBackStack();

        inOrder.verify(mPilot, never()).onFinish();
        inOrder.verify(mPilot).onSaveInstanceState(any(Bundle.class));
        inOrder.verify(mPilot).onStop();
        verifyNoMoreInteractions(mPilot);

        reset(mPilot);

        destroyActivity();

        inOrder.verify(mPilot).removeView();
        inOrder.verify(mPilot).onFinish();
        verifyNoMoreInteractions(mPilot);
    }

    @Test
    public void shouldRestartKilledPilotWithSavedState() throws Exception {
        launchActivity();

        TestBaseActivity oldActivity = mActivity;
        DummyPilot oldPilot = mActivity.mPilot;
        DummyView oldView = mActivity.mView;

        Bundle savedState = moveActivityToBackStack();

        destroyActivity();

        setUp();

        assertThat(mActivity).isNotSameAs(oldActivity);
        assertThat(mActivity.mView).isNotSameAs(oldView);
        assertThat(mPilot).isNotSameAs(oldPilot);

        launchActivityFromSavedState(savedState);

        InOrder inOrder = inOrder(mPilot);

        inOrder.verify(mPilot).setView(mActivity.mView);
        inOrder.verify(mPilot).initDummy();
        inOrder.verify(mPilot).onCreate(savedState);
        inOrder.verify(mPilot).onStart(mActivity);
        inOrder.verify(mPilot).onRestoreInstanceState(savedState);
        verifyNoMoreInteractions(mPilot);
    }
    //endregion

    @Test
    public void shouldFinishActivityIfPilotDoesNotConsumeBackPress() throws Exception {
        launchActivity();
        when(mPilot.onBackButtonPressed()).thenReturn(false);
        mActivity.onBackPressed();
        assertThat(mActivity.isFinishing()).isTrue();
    }

    @Test
    public void shouldNotFinishActivityIfPilotDoesConsumeBackPress() throws Exception {
        launchActivity();
        when(mPilot.onBackButtonPressed()).thenReturn(true);
        mActivity.onBackPressed();
        assertThat(mActivity.isFinishing()).isFalse();
    }

    //region Activity Controlling
    private void launchActivity() {
        mActivityCtrl
                .create()
                .start()
                .resume();
    }

    private void changeConfiguration() {
        Bundle outState = new Bundle();
        ShadowActivity shadowActivity = (ShadowActivity) ShadowExtractor.extract(mActivity);
        shadowActivity.changeConfigurations();
        mActivityCtrl
                .pause()
                .saveInstanceState(outState)
                .stop();
        Object nci = mActivity.onRetainNonConfigurationInstance();
        mActivityCtrl.destroy();

        mActivityCtrl = Robolectric.buildActivity(TestBaseActivity.class);
        mActivity = mActivityCtrl.get();
        shadowActivity = (ShadowActivity) ShadowExtractor.extract(mActivity);
        shadowActivity.setLastNonConfigurationInstance(nci);
        mActivityCtrl.create().start().restoreInstanceState(outState).resume();
    }

    private void launchActivityFromSavedState(Bundle savedState) {
        mActivityCtrl
                .create(savedState)
                .start()
                .restoreInstanceState(savedState)
                .resume();
    }

    private void finishActivity() {
        ShadowActivity shadowActivity = (ShadowActivity) ShadowExtractor.extract(mActivity);
        shadowActivity.resetIsChangingConfigurations();
        shadowActivity.finish();
        mActivityCtrl
                .pause()
                .stop()
                .destroy();
    }

    private Bundle moveActivityToBackStack() {
        Bundle outState = new Bundle();
        ShadowActivity shadowActivity = (ShadowActivity) ShadowExtractor.extract(mActivity);
        shadowActivity.resetIsChangingConfigurations();
        shadowActivity.resetIsFinishing();
        mActivityCtrl
                .pause()
                .saveInstanceState(outState)
                .stop();

        return outState;
    }

    private void moveToAndFromBackStack() {
        Bundle outState = new Bundle();
        ShadowActivity shadowActivity = (ShadowActivity) ShadowExtractor.extract(mActivity);
        shadowActivity.resetIsChangingConfigurations();
        mActivityCtrl
                .pause()
                .saveInstanceState(outState)
                .stop()
                .start()
                .restoreInstanceState(outState);
    }

    private void destroyActivity() {
        ShadowActivity shadowActivity = (ShadowActivity) ShadowExtractor.extract(mActivity);
        shadowActivity.resetIsChangingConfigurations();
        mActivityCtrl
                .destroy();
    }
    //endregion

    //region Test Classes
    interface DummyView {

    }

    static class DummyPilot extends Pilot<DummyView> {
        public void initDummy() {
        }
    }

    static class TestBaseActivity extends BaseActivity {

        @Inject
        DummyPilot mPilot;

        DummyView mView;

        public TestBaseActivity() {
            super();
            mView = new DummyView() {
            };
        }

        @Override
        protected BaseActivityComponent createComponent() {
            return sComponent;
        }

        @Override
        protected void inject(BaseActivityComponent component) {
            ((MockComponent) component).inject(this);
        }

        @Override
        protected int getLayoutResource() {
            return R.layout.dummy_layout;
        }

        @Override
        protected void setPilotViews() {
            setPilotView(mPilot, mView);
        }

        @Override
        protected void initPilots() {
            mPilot.initDummy();
        }

        @Override
        protected void registerPilots() {
            registerPilots(mPilot);
        }

        public DummyView getView() {
            return mView;
        }
    }
    //endregion

    //region Dependecy Injection
    @Module
    static class MockModule {
        @Provides
        @PerView
        public DummyPilot providePilot() {
            return mock(DummyPilot.class);
        }
    }

    @PerView
    @Component(
            dependencies = BaseMockComponent.class,
            modules = MockModule.class
    )
    public interface MockComponent extends BaseActivityComponent {

        void inject(TestBaseActivity activity);

        void inject(BaseActivityTest activity);
    }
    //endregion
}