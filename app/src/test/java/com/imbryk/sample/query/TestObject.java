package com.imbryk.sample.query;

import java.util.List;

public class TestObject {

    public String aString;
    public int anInt;

    @QueryName("CID")
    public String checkInDate;

    public String COD;

    @QueryName("R")
    public Room room;

    @QueryName("RN")
    public List<Room> rooms;

    public static class Room {
        @QueryName("RGA")
        public int adultsCount;

        @QueryName("RGC")
        public int childrenCount;

        @QueryName("RCA")
        public List<Integer> childAges;
    }
}
