package com.imbryk.sample.query;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class QueryFromObjectTest {

    @Test
    public void buildFromObjectWithNumbers() throws Exception {
        NumberObject input = new NumberObject();
        Query systemUnderTest = Query.build(input);
        assertThat(systemUnderTest.getInt("b")).isEqualTo(2);
        assertThat(systemUnderTest.getInt("byte")).isEqualTo(3);
        assertThat(systemUnderTest.getInt("s")).isEqualTo(4);
        assertThat(systemUnderTest.getInt("short")).isEqualTo(5);
        assertThat(systemUnderTest.getInt("i")).isEqualTo(6);
        assertThat(systemUnderTest.getInt("int")).isEqualTo(7);
        assertThat(systemUnderTest.getInt("l")).isEqualTo(8);
        assertThat(systemUnderTest.getInt("long")).isEqualTo(9);
        assertThat(systemUnderTest.getFloat("f")).isEqualTo(10.1f);
        assertThat(systemUnderTest.getFloat("float")).isEqualTo(10.2f);
        assertThat(systemUnderTest.getFloat("d")).isEqualTo(11.1f);
        assertThat(systemUnderTest.getFloat("double")).isEqualTo(11.2f);
    }

    private static class NumberObject {
        byte b = 2;
        @QueryName("byte")
        Byte bB = 3;

        short s = 4;
        @QueryName("short")
        Short sS = 5;

        int i = 6;
        @QueryName("int")
        Integer iI = 7;

        long l = 8;
        @QueryName("long")
        Long lL = 9l;

        float f = 10.1f;
        @QueryName("float")
        Float fF = 10.2f;

        double d = 11.1f;
        @QueryName("double")
        double dD = 11.2f;
    }

    @Test
    public void buildFromObjectWithStrings() throws Exception {
        StringsObject input = new StringsObject();
        Query systemUnderTest = Query.build(input);
        assertThat(systemUnderTest.getString("aString")).isEqualTo("one");
        assertThat(systemUnderTest.getString("chars")).isEqualTo("two");
    }

    private static class StringsObject {
        @QueryName("aString")
        String s = "one";

        CharSequence chars = "two";
    }

    @Test
    public void buildFromObjectWithNumbersCollections() throws Exception {
        NumbersCollectionsObject input = new NumbersCollectionsObject();
        Query systemUnderTest = Query.build(input);

        QueryParam.ArrayParam intListArray = systemUnderTest.getArray("anIntList");
        assertThat(intListArray.get(0).asInt()).isEqualTo(1);
        assertThat(intListArray.get(1).asInt()).isEqualTo(2);
        assertThat(intListArray.get(2).asInt()).isEqualTo(3);
        assertThat(intListArray.get(3).asInt()).isEqualTo(4);


        Collection<Float> floatValues = input.floatCollection;
        QueryParam.ArrayParam floatCollection = systemUnderTest.getArray("floatCollection");
        assertThat(floatCollection.getSize()).isEqualTo(floatValues.size());
        assertThat(floatCollection.get(0).asFloat()).isIn(floatValues);
        assertThat(floatCollection.get(1).asFloat()).isIn(floatValues);
        assertThat(floatCollection.get(2).asFloat()).isIn(floatValues);

        QueryParam.ArrayParam intArray = systemUnderTest.getArray("intsArray");
        assertThat(intArray.get(0).asInt()).isEqualTo(2);
        assertThat(intArray.get(1).asInt()).isEqualTo(3);
        assertThat(intArray.get(2).asInt()).isEqualTo(4);

        QueryParam.ArrayParam floatObjectsArray = systemUnderTest.getArray("floatObjectsArray");
        assertThat(floatObjectsArray.get(0).asFloat()).isEqualTo(2.2f);
        assertThat(floatObjectsArray.get(1).asFloat()).isEqualTo(3.3f);
        assertThat(floatObjectsArray.get(2).asFloat()).isEqualTo(4.4f);

    }

    private static class NumbersCollectionsObject {
        @QueryName("anIntList")
        List<Integer> l = Arrays.asList(1, 2, 3, 4);

        Collection<Float> floatCollection = new HashSet<>();

        int[] intsArray = new int[] {2,3,4};
        Float[] floatObjectsArray = new Float[] {2.2f,3.3f,4.4f};

        public NumbersCollectionsObject() {
            floatCollection.add(1.2f);
            floatCollection.add(2.3f);
            floatCollection.add(3.4f);
        }
    }

    @Test
    public void buildFromObjectWithStringsCollections() throws Exception {
        StringCollectionsObject input = new StringCollectionsObject();
        Query systemUnderTest = Query.build(input);

        QueryParam.ArrayParam stringList = systemUnderTest.getArray("aStringList");
        assertThat(stringList.get(0).asString()).isEqualTo("one");
        assertThat(stringList.get(1).asString()).isEqualTo("two");
        assertThat(stringList.get(2).asString()).isEqualTo("three");


        Collection<CharSequence> charSequences = input.charSequences;
        QueryParam.ArrayParam charSequencesCollection = systemUnderTest.getArray("charSequences");
        assertThat(charSequencesCollection.getSize()).isEqualTo(charSequences.size());
        assertThat(charSequencesCollection.get(0).asString()).isIn(charSequences);
        assertThat(charSequencesCollection.get(1).asString()).isIn(charSequences);


        QueryParam.ArrayParam stringsArray = systemUnderTest.getArray("stringsArray");
        assertThat(stringsArray.get(0).asString()).isEqualTo("dolor");
        assertThat(stringsArray.get(1).asString()).isEqualTo("sit");
        assertThat(stringsArray.get(2).asString()).isEqualTo("amet");

    }

    private static class StringCollectionsObject {
        @QueryName("aStringList")
        public List<String> strings = Arrays.asList("one", "two", "three");

        Collection<CharSequence> charSequences = new HashSet<>();

        String[] stringsArray = new String[] {"dolor", "sit", "amet"};

        public StringCollectionsObject() {
            charSequences.add("lorem");
            charSequences.add("ipsum");
        }
    }

    @Test
    public void buildFromObjectWithMaps() throws Exception {
        MapsObject input = new MapsObject();
        Query systemUnderTest = Query.build(input);

        QueryParam.ArrayParam strings = systemUnderTest.getArray("aStringMap");
        assertThat(strings.get("first").asString()).isEqualTo("one");
        assertThat(strings.get("second").asString()).isEqualTo("two");
        assertThat(strings.get("third").asString()).isEqualTo("three");

        QueryParam.ArrayParam ints = systemUnderTest.getArray("ints");
        assertThat(ints.get("first").asInt()).isEqualTo(1);
        assertThat(ints.get("second").asInt()).isEqualTo(2);
        assertThat(ints.get("third").asInt()).isEqualTo(3);

        QueryParam.ArrayParam floats = systemUnderTest.getArray("floats");
        assertThat(floats.get("first").asFloat()).isEqualTo(1.1f);
        assertThat(floats.get("second").asFloat()).isEqualTo(2.2f);
        assertThat(floats.get("third").asFloat()).isEqualTo(3.3f);

    }

    private static class MapsObject {
        @QueryName("aStringMap")
        Map<String, String> strings = new HashMap<>();

        Map<String, Integer> ints = new Hashtable<>();
        Map<String, Float> floats = new TreeMap<>();

        public MapsObject() {
            strings.put("first", "one");
            strings.put("second", "two");
            strings.put("third", "three");
            ints.put("first", 1);
            ints.put("second", 2);
            ints.put("third", 3);
            floats.put("first", 1.1f);
            floats.put("second", 2.2f);
            floats.put("third", 3.3f);
        }
    }

    @Test
    public void buildFromComplexObject() throws Exception {
        ComplexObject input = new ComplexObject();
        Query systemUnderTest = Query.build(input);

        float aFloat = systemUnderTest.getFloat("aFloat");
        assertThat(aFloat).isEqualTo(1.1f);

        String aString = systemUnderTest.getString("aString");
        assertThat(aString).isEqualTo("string");

        QueryParam.ArrayParam inner = systemUnderTest.getArray("innerObject");
        assertThat(inner).isNotNull();

        QueryParam innerInt = inner.get("anInt");
        assertThat(innerInt.asInt()).isEqualTo(1);

        QueryParam.ArrayParam innerList = inner.getArray("aList");
        assertThat(innerList.get(0).asString()).isEqualTo("one");
        assertThat(innerList.get(1).asString()).isEqualTo("two");
    }

    private static class ComplexObject {
        InnerObject innerObject = new InnerObject();
        Float aFloat = 1.1f;
        String aString = "string";
    }

    private static class InnerObject {
        int anInt = 1;
        List<String> aList = Arrays.asList("one", "two");
    }

    @Test
    public void buildFromObjectWithComplexCollectionAndMap() throws Exception {
        ComplexCollectionObject input = new ComplexCollectionObject();
        Query systemUnderTest = Query.build(input);

        QueryParam.ArrayParam aCollection = systemUnderTest.getArray("aCollection");
        assertThat(aCollection.getArray(0).get("value").asInt()).isEqualTo(1);
        assertThat(aCollection.getArray(1).get("value").asInt()).isEqualTo(2);

        QueryParam.ArrayParam aMap = systemUnderTest.getArray("aMap");
        assertThat(aMap.getArray("one").get("value").asInt()).isEqualTo(1);
        assertThat(aMap.getArray("two").get("value").asInt()).isEqualTo(2);

        QueryParam.ArrayParam anArray = systemUnderTest.getArray("anArray");
        assertThat(anArray.getArray(0).get("value").asInt()).isEqualTo(5);
        assertThat(anArray.getArray(1).get("value").asInt()).isEqualTo(6);

    }

    private static class ComplexCollectionObject {
        Collection<WrapperObject> aCollection = Arrays.asList(new WrapperObject(1), new WrapperObject(2));
        Map<String, WrapperObject> aMap = new HashMap<>();

        WrapperObject[] anArray = new WrapperObject[]{new WrapperObject(5), new WrapperObject(6)};

        public ComplexCollectionObject() {
            aMap.put("one", new WrapperObject(1));
            aMap.put("two", new WrapperObject(2));
        }
    }

    private static class WrapperObject {
        int value;

        public WrapperObject(int i) {
            value = i;
        }
    }

}