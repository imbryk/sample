package com.imbryk.sample.query;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;


public class QueryFromStringTest {

    @Test
    public void shouldAllowToPassQueryInConstructor() throws Exception {
        Query systemUnderTest = Query.fromString("a=1&b=B&c[]=1&c[]=2");

        assertThat(systemUnderTest).isNotNull();
    }

    @Test
    public void parseString() throws Exception {
        Query systemUnderTest = Query.fromString("a=abc&b=XYZ");

        assertThat(systemUnderTest.getString("a")).isEqualTo("abc");
        assertThat(systemUnderTest.getString("b")).isEqualTo("XYZ");
    }

    @Test
    public void parseInteger() throws Exception {
        Query systemUnderTest = Query.fromString("a=1&b=2&c=-3");

        assertThat(systemUnderTest.getInt("a")).isEqualTo(1);
        assertThat(systemUnderTest.getInt("b")).isEqualTo(2);
        assertThat(systemUnderTest.getInt("c")).isEqualTo(-3);
    }

    @Test
    public void parseFloat() throws Exception {
        Query systemUnderTest = Query.fromString("a=1.2&b=0.1&c=.1&d=-0.1&e=-.1");

        assertThat(systemUnderTest.getFloat("a")).isEqualTo(1.2f);
        assertThat(systemUnderTest.getFloat("b")).isEqualTo(.1f);
        assertThat(systemUnderTest.getFloat("c")).isEqualTo(.1f);
        assertThat(systemUnderTest.getFloat("d")).isEqualTo(-.1f);
        assertThat(systemUnderTest.getFloat("e")).isEqualTo(-.1f);
    }

    @Test
    public void parseArray() throws Exception {
        Query systemUnderTest = Query.fromString("a[0]=10&b[1]=21&c[3]=three&a[1]=11&c[1]=one&b[0]=20");

        QueryParam.ArrayParam arrayA = systemUnderTest.getArray("a");
        QueryParam.ArrayParam arrayB = systemUnderTest.getArray("b");
        QueryParam.ArrayParam arrayC = systemUnderTest.getArray("c");

        assertThat(arrayA.get(0).asInt()).isEqualTo(10);
        assertThat(arrayA.get(1).asInt()).isEqualTo(11);

        assertThat(arrayB.get(0).asInt()).isEqualTo(20);
        assertThat(arrayB.get(1).asInt()).isEqualTo(21);

        assertThat(arrayC.get(0)).isNull();
        assertThat(arrayC.get(1).asString()).isEqualTo("one");
        assertThat(arrayC.get(2)).isNull();
        assertThat(arrayC.get(3).asString()).isEqualTo("three");
    }

    @Test
    public void parseComplexArray() throws Exception {
        Query systemUnderTest = Query.fromString("a[0][0]=a00&a[1][0]=a10&a[1][1]=a11&a[0][1]=a01");

        QueryParam.ArrayParam topArray = systemUnderTest.getArray("a");
        QueryParam.ArrayParam innerArray0 = (QueryParam.ArrayParam) topArray.get(0);
        QueryParam.ArrayParam innerArray1 = (QueryParam.ArrayParam) topArray.get(1);

        assertThat(innerArray0.get(0).asString()).isEqualTo("a00");
        assertThat(innerArray0.get(1).asString()).isEqualTo("a01");
        assertThat(innerArray1.get(0).asString()).isEqualTo("a10");
        assertThat(innerArray1.get(1).asString()).isEqualTo("a11");
    }

    @Test
    public void parseObjects() throws Exception {
        Query systemUnderTest = Query.fromString("a[x]=aX&b[X]=bigX&c[A]=1&a[y]=aY&b[x]=smallX&c[B]=2");
        QueryParam.ArrayParam arrayA = systemUnderTest.getArray("a");
        QueryParam.ArrayParam arrayB = systemUnderTest.getArray("b");
        QueryParam.ArrayParam arrayC = systemUnderTest.getArray("c");

        assertThat(arrayA.get("x").asString()).isEqualTo("aX");
        assertThat(arrayA.get("y").asString()).isEqualTo("aY");

        assertThat(arrayB.get("x").asString()).isEqualTo("smallX");
        assertThat(arrayB.get("X").asString()).isEqualTo("bigX");

        assertThat(arrayC.get("A").asInt()).isEqualTo(1);
        assertThat(arrayC.get("B").asInt()).isEqualTo(2);
    }

    @Test
    public void parseComplexObjects() throws Exception {
        Query systemUnderTest = Query.fromString("a[X][first]=100&a[Y][first]=firstY&a[X][sec]=200&a[Y][sec]=secY");
        QueryParam.ArrayParam topArray = systemUnderTest.getArray("a");
        QueryParam.ArrayParam innerX = (QueryParam.ArrayParam) topArray.get("X");
        QueryParam.ArrayParam innerY = (QueryParam.ArrayParam) topArray.get("Y");

        assertThat(innerX.get("first").asInt()).isEqualTo(100);
        assertThat(innerX.get("sec").asInt()).isEqualTo(200);

        assertThat(innerY.get("first").asString()).isEqualTo("firstY");
        assertThat(innerY.get("sec").asString()).isEqualTo("secY");
    }

    @Test
    public void parseComplexData() throws Exception {
        Query systemUnderTest = Query.fromString("CID=2017-02-04&COD=2017-02-05&RN[0][RGA]=2&RN[0][RGC]=2&RN[0][RCA][0]=2&RN[0][RCA][1]=4&RN[1][RGA]=1&RN[1][RGC]=1&RN[1][RCA][0]=0&RN[2][RGA]=3&RN[2][RGC]=3&RN[2][RCA][0]=0&RN[2][RCA][1]=2&RN[2][RCA][2]=1");

        assertThat(systemUnderTest.getString("CID")).isEqualTo("2017-02-04");
        assertThat(systemUnderTest.getString("COD")).isEqualTo("2017-02-05");

        QueryParam.ArrayParam RN = systemUnderTest.getArray("RN");
        QueryParam.ArrayParam RN0 = RN.getArray(0);
        QueryParam.ArrayParam RN1 = RN.getArray(1);
        QueryParam.ArrayParam RN2 = RN.getArray(2);

        assertThat(RN0.get("RGA").asInt()).isEqualTo(2);
        assertThat(RN1.get("RGA").asInt()).isEqualTo(1);
        assertThat(RN2.get("RGA").asInt()).isEqualTo(3);

        assertThat(RN0.get("RGC").asInt()).isEqualTo(2);
        assertThat(RN1.get("RGC").asInt()).isEqualTo(1);
        assertThat(RN2.get("RGC").asInt()).isEqualTo(3);

        QueryParam.ArrayParam RCA0 = RN0.getArray("RCA");
        QueryParam.ArrayParam RCA1 = RN1.getArray("RCA");
        QueryParam.ArrayParam RCA2 = RN2.getArray("RCA");

        assertThat(RCA0.get(0).asInt()).isEqualTo(2);
        assertThat(RCA0.get(1).asInt()).isEqualTo(4);

        assertThat(RCA1.get(0).asInt()).isEqualTo(0);

        assertThat(RCA2.get(0).asInt()).isEqualTo(0);
        assertThat(RCA2.get(1).asInt()).isEqualTo(2);
        assertThat(RCA2.get(2).asInt()).isEqualTo(1);
    }

    @Test
    public void parseToClass() throws Exception {
        Query systemUnderTest = Query.fromString("aString=someString&anInt=123&CID=2017-02-04&COD=2017-02-05&" +
                "R[RGA]=2&R[RGC]=1&" +
                "RN[0][RGA]=2&RN[0][RGC]=2&RN[0][RCA][0]=2&RN[0][RCA][1]=4&" +
                "RN[1][RGA]=1&RN[1][RGC]=1&RN[1][RCA][0]=0&" +
                "RN[2][RGA]=3&RN[2][RGC]=3&RN[2][RCA][0]=0&RN[2][RCA][1]=2&RN[2][RCA][2]=1");
        TestObject result = systemUnderTest.toClass(TestObject.class);

        assertThat(result.aString).isEqualTo("someString");
        assertThat(result.anInt).isEqualTo(123);
        assertThat(result.checkInDate).isEqualTo("2017-02-04");
        assertThat(result.COD).isEqualTo("2017-02-05");

        TestObject.Room room = result.room;
        assertThat(room).isNotNull();
        assertThat(room.adultsCount).isEqualTo(2);
        assertThat(room.childrenCount).isEqualTo(1);

        List<TestObject.Room> rooms = result.rooms;
        assertThat(rooms).isNotNull();
        assertThat(rooms).hasSize(3);

        TestObject.Room room0 = rooms.get(0);
        assertThat(room0).isNotNull();
        assertThat(room0.adultsCount).isEqualTo(2);
        assertThat(room0.childrenCount).isEqualTo(2);
        List<Integer> childAges0 = room0.childAges;
        assertThat(childAges0).hasSize(2);
        assertThat(childAges0.get(0)).isEqualTo(2);
        assertThat(childAges0.get(1)).isEqualTo(4);

        TestObject.Room room1 = rooms.get(1);
        assertThat(room1).isNotNull();
        assertThat(room1.adultsCount).isEqualTo(1);
        assertThat(room1.childrenCount).isEqualTo(1);
        List<Integer> childAges1 = room1.childAges;
        assertThat(childAges1).hasSize(1);
        assertThat(childAges1.get(0)).isEqualTo(0);

        TestObject.Room room2 = rooms.get(2);
        assertThat(room2).isNotNull();
        assertThat(room2.adultsCount).isEqualTo(3);
        assertThat(room2.childrenCount).isEqualTo(3);
        List<Integer> childAges2 = room2.childAges;
        assertThat(childAges2).hasSize(3);
        assertThat(childAges2.get(0)).isEqualTo(0);
        assertThat(childAges2.get(1)).isEqualTo(2);
        assertThat(childAges2.get(2)).isEqualTo(1);
    }

}