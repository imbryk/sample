package com.imbryk.sample.query;

import org.assertj.core.api.Java6SoftAssertions;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class QueryStringFromObjectTest {

    @Test
    public void createStringFromFlatClass() throws Exception {
        FlatObject input = new FlatObject();
        Query systemUnderTest = Query.build(input);


        String expectedQuery = "anInt=1&aFloat=1.1&aString=string";
        assertQueryEquals(systemUnderTest.toQueryString(), expectedQuery);
    }

    private static class FlatObject {
        int anInt = 1;
        float aFloat = 1.1f;
        String aString = "string";
    }

    @Test
    public void createStringFromClassWithList() throws Exception {
        ListObject input = new ListObject();
        Query systemUnderTest = Query.build(input);


        String expectedQuery = "anInt=1&strings[0]=one&strings[1]=two&ints[0]=1&ints[1]=2";
        assertQueryEquals(systemUnderTest.toQueryString(), expectedQuery);
    }

    private static class ListObject {
        int anInt = 1;
        List<String> strings = Arrays.asList("one","two");
        List<Integer> ints = Arrays.asList(1,2);
    }

    @Test
    public void createStringFromClassWithMap() throws Exception {
        MapObject input = new MapObject();
        Query systemUnderTest = Query.build(input);


        String expectedQuery = "anInt=1&strings[first]=one&strings[second]=two&ints[first]=1&ints[second]=2";
        assertQueryEquals(systemUnderTest.toQueryString(), expectedQuery);
    }

    private static class MapObject {
        int anInt = 1;
        Map<String, String> strings = new HashMap<>();
        Map<String, Integer> ints = new HashMap<>();

        public MapObject() {
            strings.put("first", "one");
            strings.put("second", "two");
            ints.put("first", 1);
            ints.put("second", 2);
        }
    }

    @Test
    public void createStringFromClass() throws Exception {
        TestObject input = createTestObject();
        Query systemUnderTest = Query.build(input);

        String expectedQuery = "aString=someString&anInt=123&CID=2017-02-04&COD=2017-02-05&" +
                "R[RGA]=2&R[RGC]=1&" +
                "RN[0][RGA]=2&RN[0][RGC]=2&RN[0][RCA][0]=2&RN[0][RCA][1]=4&" +
                "RN[1][RGA]=1&RN[1][RGC]=1&RN[1][RCA][0]=0&" +
                "RN[2][RGA]=3&RN[2][RGC]=3&RN[2][RCA][0]=0&RN[2][RCA][1]=2&RN[2][RCA][2]=1";

        assertQueryEquals(systemUnderTest.toQueryString(), expectedQuery);
    }

    private TestObject createTestObject() {

        TestObject testObject = new TestObject();

        testObject.aString = "someString";
        testObject.anInt = 123;
        testObject.checkInDate = "2017-02-04";
        testObject.COD = "2017-02-05";

        TestObject.Room room = new TestObject.Room();
        room.adultsCount = 2;
        room.childrenCount = 1;
        testObject.room = room;


        TestObject.Room room0 = new TestObject.Room();
        room0.adultsCount = 2;
        room0.childrenCount = 2;
        room0.childAges = Arrays.asList(2, 4);

        TestObject.Room room1 = new TestObject.Room();
        room1.adultsCount = 1;
        room1.childrenCount = 1;
        room1.childAges = Collections.singletonList(0);

        TestObject.Room room2 = new TestObject.Room();
        room2.adultsCount = 3;
        room2.childrenCount = 3;
        room2.childAges = Arrays.asList(0, 2, 1);

        testObject.rooms = Arrays.asList(room0, room1, room2);

        return testObject;
    }

    private void assertQueryEquals(String realQuery, String expectedQuery) {
        Java6SoftAssertions softly = new Java6SoftAssertions();
        softly.assertThat(realQuery.length())
                .overridingErrorMessage("Expected query length: %d  but was: %d\n" +
                        "Expected query:\n\t%s\nbut actual query was:\n\t%s",
                        expectedQuery.length(), realQuery.length(),
                        expectedQuery, realQuery
                )
                .isEqualTo(expectedQuery.length());
        String[] realQueryParams = realQuery.split("&");
        List<String> expectedQueryParams = Arrays.asList(expectedQuery.split("&"));
        for (String param: realQueryParams) {
            softly.assertThat(param)
                    .overridingErrorMessage("Expecting: %s to be in: %s",param, expectedQuery)
                    .isIn(expectedQueryParams);
        }
        softly.assertAll();
    }
}