package com.imbryk.sample.dagger;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = BaseMockModule.class
)
public interface BaseMockComponent {
    Context provideContext();
}
