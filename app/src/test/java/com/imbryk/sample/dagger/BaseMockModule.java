package com.imbryk.sample.dagger;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.mock;

@Module
class BaseMockModule {
    @Singleton
    @Provides
    public Context provideContext() {
        return mock(Context.class);
    }
}
